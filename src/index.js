import React, {Suspense, lazy} from "react";
import ReactDOM from "react-dom";
import {Layout} from "./app/@core/utility/context/Layout";
import * as serviceWorker from "./serviceWorker";
import "./index.scss";
import 'font-awesome/css/font-awesome.min.css'
import {Route, Router, Switch} from "react-router";
import {history} from "./history";

const LazyApp = lazy(() => import("./app/App"));
const LazyLandingApp = lazy(() => import("./landing/App"));

ReactDOM.render(
  <Suspense fallback={<div/>}>
    <Layout>
      <Router history={history}>
        <Switch>
          <Route path={'/dashboard*'}>
            <LazyApp/>
          </Route>
          <Route exact path={'/*'}>
            <LazyLandingApp/>
          </Route>
        </Switch>
      </Router>
    </Layout>
  </Suspense>,
  document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more landing service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
