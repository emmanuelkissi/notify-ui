import React from 'react';
import {Code, Bell} from "react-feather"
import {
  zoomIn
} from 'react-animations';
import styled, {keyframes} from 'styled-components';
import ScrollableAnchor from 'react-scrollable-anchor'

// import images
import bgHomeSection from '../../assets/img/svg/notify-1.svg';
import bgHomeSection2 from '../../assets/img/svg/notify-6.svg';
import bgHomeSection3 from '../../assets/img/svg/notify-7.svg';
import basic from '../../assets/img/basic.svg';
import premium from '../../assets/img/premium.svg';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Media,
  Container,
  Button
} from "reactstrap";
import ChatBot from 'react-simple-chatbot';


function Section() {
  const steps = [
    {
      id: '0',
      message: 'Bienvenue! Comment pourrais-je vous aider?',
      trigger: '1',
    },
    {
      id: '1',
      message: 'A bientôt!',
      end: true,
    },
  ];

  return (
    <React.Fragment>
      <section className="bg-half-170 d-table w-100" id="home" style={{padding: "90px", fontFamily: "Jost"}}>
        <Container fluid={true}>
          <Row>
            <Col lg="8" md="8" sm="12" className="mt-3 mb-1">
            <BouncyDiv>
              <img
                style={{
                  margin: "auto",
                  width: '100%',
                  bottom: "0"
                }}
                src={bgHomeSection}
                alt=""
              />
            </BouncyDiv>
            </Col>
            <Col lg="4" md="4" sm="12"  style={{
                  margin: "auto",
                  fontSize:'18px',
                }}>
                  <Row>
                    <Col >
                                          <img
                                style={{
                                  margin: "auto",
                                  width: '100%',
                                  bottom: "0"
                                }}
                                src={bgHomeSection2}
                                alt=""
                              />
                    </Col>
                    <Col>
                    Wouhou Envoyer facilement vos notifications avec Notify 
                    </Col>
                  </Row>
             
            </Col>
          </Row>
        </Container>
        <div className="m-auto text-center" id="features">
          
          <Media>
              
              <Media body>
              <h1 className="display-3 mb-3" style={{
                                  marginTop: "60px",
                                 
                                }}>Notify</h1>
               
                 Est un sytème de push notificatif Dévellopé par AkilTechnologie ,basé sur les nouvelle technologie en vu de permettre au dévellopeur et au particulier de rester en 
             contacte permanant avec leur client . De ce fait le système dispose d'api et d'inteface d'utlisateur ain d'assurer 
             au mieux la prise en main .
              </Media>
          </Media>
          
        </div>
      </section>
      <ScrollableAnchor id={'features'}>
        <section style={{position: "relative", top: "-5em"}}>
          <Row className="text-center mt-5">
            <Col>
            <h2  >Fonctionnalités</h2>
            </Col>
          </Row>
          <Row className="justify-content-md-center mt-5">
            <Col lg="3" md="6" sm="12">
              <BouncyDiv>
                <Card style={{borderRadius: "20px", height: "17em"}}>
                  <CardHeader className="mx-auto">
                    <div className="avatar avatar-stats p-50 m-0 bg-info"
                         style={{padding: "10px", borderRadius: "30px"}}><Bell
                      style={{fontSize: "1.8em", color: "white"}} size={40}/></div>
                  </CardHeader>
                  <CardBody className="text-center">
                    <h4 className={"mb-4"}>Push SMS</h4>
                    <p>Rester en contacte avec vos clients à partir de nos push SMS rapide en toute sécurité</p>
                  </CardBody>
                </Card>
              </BouncyDiv>

            </Col>
            <Col lg="3" md="6" sm="12">
              <BouncyDiv>
                <Card style={{borderRadius: "20px", height: "17em"}}>
                <CardHeader className="mx-auto">
                    <div className="avatar avatar-stats p-50 m-0 bg-info"
                         style={{padding: "10px", borderRadius: "30px"}}><Bell
                      style={{fontSize: "1.8em", color: "white"}} size={40}/></div>
                  </CardHeader>
                  <CardBody className="text-center">
                    <h4 className={"mb-4"}>Push Email</h4>
                    <p>N'oublier pas vos membres , tener leur informer des offres du moment avec l'offre Push Email</p>
                  </CardBody>
                </Card>
              </BouncyDiv>

            </Col>
            <Col lg="3" md="6" sm="12">
              <BouncyDiv>
                <Card style={{borderRadius: "20px", height: "17em"}}>
                <CardHeader className="mx-auto">
                    <div className="avatar avatar-stats p-50 m-0 bg-info"
                         style={{padding: "10px", borderRadius: "30px"}}><Bell
                      style={{fontSize: "1.8em", color: "white"}} size={40}/></div>
                  </CardHeader>
                  <CardBody className="text-center">
                    <h4 className={"mb-4"}>Push Web</h4>
                    <p>Le monde est aujourd'hui le plus connecté que possible , avec les push Web , notifier des données au Smartphone , Pc et tous autres outils à traver le web </p>
                  </CardBody>
                </Card>
              </BouncyDiv>

            </Col>
            
           
            <Col lg="3" md="6" sm="12">
              <BouncyDiv>
                <Card style={{borderRadius: "20px", height: "17em"}}>
                  <CardHeader className="mx-auto">
                    <div className="avatar avatar-stats p-50 m-0 bg-info"
                         style={{padding: "10px", borderRadius: "30px"}}><Code
                      style={{fontSize: "1.8em", color: "white"}} size={40}/></div>
                  </CardHeader>
                  <CardBody className="text-center">
                    <h4 className={"mb-4"}>API REST</h4>
                    <p>Etes vous un developpeur, les systèmes est disponibles sous forme D'API qui vous aidera à l'utilisation dans vos appications.</p>
                  </CardBody>
                </Card>
              </BouncyDiv>
            </Col>
          </Row>
        </section>
      </ScrollableAnchor>

      <section>
        <Container>
            <Row>
              <Col lg="10" md="10" sm="10">
              <img
                style={{
                  margin: "auto",
                  width: '100%',
                  bottom: "0"
                }}
                src={bgHomeSection3}
                alt=""
              />
              </Col>
              <Col lg="2" md="2" sm="2" className="m-auto">
                            <p>
                                Gérer vos notifications en toute simplicité avec NOTIFY , aussi facilement .
                            </p>
                              <Button>
                                  Essayer
                              </Button>
              </Col>
            </Row>
        </Container>
      </section>
      <section id="pricing">
        <Row className="text-center mt-5">
              <Col>
              <h2  >Tarifications</h2>
              </Col>
        </Row>
        <Row className="justify-content-md-center">
          <Col lg="3" md="6" sm="12">
            <Card style={{borderRadius: "20px"}}>
              <CardBody className="text-center p-5">
                <img src={basic} alt="" style={{width: "7vw", marginBottom: "4vh"}}/>
                <h4 style={{marginBottom: "4vh"}}>GRATUIT</h4>
                <p>Lorem ipsum dolor sit amet, consecteur</p>
                <div className="getting-started-button" style={{marginLeft: "10px"}}>
                  <a href='/dashboard/apps' id="buyButton" className="btn btn-primary">Payer</a>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="12">
            <Card style={{borderRadius: "20px"}}>
              <CardBody className="text-center p-5">
                <img src={premium} alt="" style={{width: "7vw", marginBottom: "4vh"}}/>
                <h4 style={{marginBottom: "4vh"}}>STANDARD</h4>
                <p>Lorem ipsum dolor sit amet, consecteur</p>
                <div className="getting-started-button" style={{marginLeft: "10px"}}>
                  <a href='/dashboard/apps' id="buyButton" className="btn btn-primary">Payer</a>
                </div>
              </CardBody>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="12">
            <Card style={{borderRadius: "20px"}}>
              <CardBody className="text-center p-5">
                <img src={basic} alt="" style={{width: "7vw", marginBottom: "4vh"}}/>
                <h4 style={{marginBottom: "4vh"}}>PREMIUM</h4>
                <p>Lorem ipsum dolor sit amet, consecteur</p>
                <div className="getting-started-button" style={{marginLeft: "10px"}}>
                  <a href='/dashboard/apps' id="buyButton" className="btn btn-primary">Payer</a>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

      </section>
      <ChatBot
        className="rsc-cb"
        headerTitle='Message'
        hideUserAvatar='true'
        hideBotAvatar='true'
        width='650px'
        color="red"
        steps={steps}
        floating={true}
        placeholder={"Entrer votre message"}
        user={true}
      />
    </React.Fragment>
  );
}

const bounceAnimation = keyframes`${zoomIn}`;

const BouncyDiv = styled.div`
  animation: 1s ${bounceAnimation};
`;


export default Section;
