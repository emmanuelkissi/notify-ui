import React from 'react';

const Home = React.lazy(() => {
  return import('./views/Home');
});

const routes = [
  {path: '/', component: Home}
];

export default routes;
