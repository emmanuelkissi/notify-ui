import React, {useEffect, useState} from 'react';
import {withRouter} from 'react-router-dom';
import Loader from "../components/Loader";

const Topbar = React.lazy(() => import('./components/Topbar'));
const Footer = React.lazy(() => import('./components/Footer'));

function Layout({children}) {
  const [isLoading, setIsLoading] = useState(true);

  useEffect( () => {
    const timer = wait(5000)
    return () => {
      clearTimeout(timer)
    }
  }, []);


  const wait = (milliseconds = 2000) => {
    return setTimeout(() => setIsLoading(false), milliseconds)
  };

  return (
    <React.Fragment>
      {isLoading === true ? <Loader/> :
        <React.Fragment>
          <Topbar/>
          {children}
          <Footer/>
        </React.Fragment>
      }
    </React.Fragment>
  );
}

export default withRouter(Layout);
