import React from "react";
import styled from "styled-components";
import Avatar from "../../components/AvatarComponent"
import {Facebook, Instagram, Twitter, Phone, Mail, MapPin} from "react-feather"


function Footer() {
  return (
    <div className="App" id="contact">
      <FooterContainer className="main-footer bg-secondary text-white" >
        <div className="footer-middle">
          <div className="container">
            <div className="row">
              <div className="col-md-3 col-sm-6">
                <h4 className="mb-3">Qui sommes nous?</h4>
                <p>Notify est un système développé par Akil technologie , l'objectif est de regrouper les différents systèmes notificatif en un seul</p>
              </div>
              <div className="col-md-3 col-sm-6">
                <h4 className="mb-3">Adresse</h4>
                <ul className="list-unstyled">
                  <li className="mt-2 text-white" style={{color: "#000000"}}><Phone/> (225) 224 274 06</li>
                  <li className="mt-2 text-white" style={{color: "#000000"}}><Mail/> info@akilcab.com</li>
                  <li className="mt-2 text-white" style={{color: "#000000"}}><MapPin/> 145 Voie de la 7 eme tranche</li>
                </ul>
              </div>

              <div className="col-md-3 col-sm-6">
              </div>

              <div className="col-md-3 col-sm-6 text-white">
                <h4 className="mb-3">Suivez nous</h4>
                <Avatar className="mr-1" color="primary" icon={<Facebook/>}/>
                <Avatar className="mr-1" icon={<Twitter/>} color="info"/>
                <Avatar icon={<Instagram/>} color="success"/>
              </div>
            </div>
          </div>
          {/*Footer Bottom*/}
          <div className="footer-bottom">
            <p className="text-xs-center">
              COPYRIGHT&copy;{new Date().getFullYear()} Notify. - tous droits reservés
            </p>
          </div>
        </div>
      </FooterContainer>
    </div>
  );
}

export default Footer;

const FooterContainer = styled.footer`
     .footer-middle{
         background: var(--mainDark);
         color: var(--mainWhite);
         padding-top: 3rem ;
     }
     .footer-bottom{
         text-align: center;
         padding-top: 3rem;
         padding-bottom: 2rem;
     }
     ul li a{
         color: #00CED1;
     }
     ul li {
      color: #00CED1;
  }
    `;


