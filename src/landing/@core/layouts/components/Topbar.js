import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {withRouter} from "react-router";
import {Container} from "reactstrap";

class Topbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      navLinks: [
        {id: 1, title: "Accueil", link: "/"},
        {id: 2, title: "Fonctionnalités", link: "#features"},
        {id: 3, title: "Tarification", link: "#pricing"},
        {id: 4, title: "Contact", link: "#contact"},
      ]
    };
    this.toggleLine = this.toggleLine.bind(this);
    this.openBlock.bind(this);
    this.openNestedBlock.bind(this);

  }

  toggleLine() {
    this.setState(prevState => ({isOpen: !prevState.isOpen}));
  }

  componentDidMount() {
    let matchingMenuItem = null;
    const ul = document.getElementById("top-menu");
    const items = ul.getElementsByTagName("a");
    for (let i = 0; i < items.length; ++i) {
      if (this.props.location.pathname === items[i].pathname) {
        matchingMenuItem = items[i];
        break;
      }
    }
    if (matchingMenuItem) {
      this.activateParentDropdown(matchingMenuItem);
    }
  }

  activateParentDropdown = (item) => {
    const parent = item.parentElement;
    if (parent) {
      parent.classList.add('active'); // li
      const parent1 = parent.parentElement;
      parent1.classList.add('active'); // li
      if (parent1) {
        const parent2 = parent1.parentElement;
        parent2.classList.add('active'); // li
        if (parent2) {
          const parent3 = parent2.parentElement;
          parent3.classList.add('active'); // li
          if (parent3) {
            const parent4 = parent3.parentElement;
            parent4.classList.add('active'); // li
          }
        }
      }
    }
  };

  openBlock = (level2_id) => {
    //match level 2 id with current clicked id and if id is correct then update status and open level 2 submenu
    this.setState({
      navLinks: this.state.navLinks.map(navLink => (navLink.id === level2_id ? {
        ...navLink,
        isOpenSubMenu: !navLink.isOpenSubMenu
      } : navLink))
    });
  };

  openNestedBlock = (level2_id, level3_id) => {
    const tmpLinks = this.state.navLinks;
    tmpLinks.map((tmpLink) =>
      //Match level 2 id
      tmpLink.id === level2_id ?
        tmpLink.child.map((tmpchild) =>
          //if level1 id is matched then match level 3 id
          tmpchild.id === level3_id ?
            //if id is matched then update status(level 3 sub menu will be open)
            tmpchild.isOpenNestedSubMenu = !tmpchild.isOpenNestedSubMenu
            :
            tmpchild.isOpenNestedSubMenu = false
        )
        :
        false
    );
    this.setState({navLinks: tmpLinks});
  };

  render() {
    return (
      <React.Fragment>
        <header
          id="topnav"
          className="defaultscroll sticky text-exo"
          style={{backgroundColor: '5885f4'}}
        >
          <Container>
            <div>
              <Link className="logo text-exo" to="/">Notify.</Link>
            </div>
            <div>
              <div className="getting-started-button" style={{marginLeft: "10px"}}>
                <a href='/dashboard/apps' className="btn btn-primary">Essayer</a>
              </div>
            </div>
            <div id="navigation" style={{display: this.state.isOpen ? "block" : "none"}}>
              <ul className="navigation-menu" id="top-menu">
                {
                  this.state.navLinks.map((navLink, key) =>
                    <li key={key}><Link to={navLink.link}>{navLink.title}</Link></li>
                  )
                }
              </ul>
            </div>
          </Container>
        </header>
      </React.Fragment>
    );
  }
}

export default withRouter(Topbar);
