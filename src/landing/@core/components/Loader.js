import React, {useEffect, useState} from "react";
import l1 from '../../../assets/img/1.svg';
import l2 from '../../../assets/img/2.svg';

const Loader = () => {
  const  [keyImgToLoad, setKeyImgToLoad] = useState(0);
  const hideSelf = () => {
    setKeyImgToLoad(keyImgToLoad + 1)
    if (keyImgToLoad === 2) {
      setKeyImgToLoad(0)
    }
  };
  useEffect(() => {
    const timer = setTimeout(() => hideSelf(), 300);

    return () => {
      clearTimeout(timer)
    }
  })


  return (<React.Fragment>
    <div className="context">
    </div>

    <div className="area">
      <ul className="circles">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
      <div className={"loader hero__title"}
           style={{height: "100%", width: "100%", position: "absolute", border: "1px solid black"}}>
        {keyImgToLoad === 0 ?
          <img alt="" src={l1} style={{display: "block", width: "15em", margin: "15em auto", marginBottom: "0"}}/> :
          <img alt="" src={l2} style={{display: "block", width: "15em", margin: "15em auto", marginBottom: "0"}}/>}
        <div className="col-sm-6 text-center" style={{margin: "auto"}}>
          <div className="loader1">
            <span>E</span>
            <span>b</span>
            <span>o</span>
            <span>o</span>
            <span>t</span>
          </div>
          <div style={{fontSize: "1.7em", fontWeight: "bold"}}>Notify.</div>
        </div>
      </div>
    </div>
  </React.Fragment>)
}

export default Loader;
