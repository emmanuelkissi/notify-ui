import React from "react"
import {
  Alert,
  Button,
  Media,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Col
} from "reactstrap"
import img from "../../../../assets/img/elements/create-profil-1.jpg"
class General extends React.Component {
  state = {
    visible: true
  }

  dismissAlert = () => {
    this.setState({
      visible: true
    })
  }

  render() {
    return (
      <React.Fragment>
        <div className="container mt-5">
        <div className="row" > 
        <div className="col-lg-8 offset-lg-2">
        <Media>
          <Media className="mr-1" left href="#">
            <Media
              className="rounded-circle"
              object
              src={img}
              alt="User"
              height="64"
              width="64"
            />
          </Media>
          <Media className="mt-25" body>
            <div className="d-flex flex-sm-row flex-column justify-content-start px-0">
              <Button.Ripple
                tag="label"
                className="mr-50 cursor-pointer"
                color="primary"
                outline
              >
               Charger une photo
                <Input type="file" name="file" id="uploadImg" hidden />
              </Button.Ripple>
              <Button.Ripple color="flat-danger">Supprimer</Button.Ripple>
            </div>
            <p className="text-muted mt-50">
              <small>Choix JPG, GIF or PNG. Max taille maxi 800kB</small>
            </p>
          </Media>
        </Media>
        <Form className="mt-2" onSubmit={e => e.preventDefault()}>
          <Row>
            <Col sm="12">
              <FormGroup>
                <Label for="userName">Nom d'utilisateur</Label>
                <Input id="userName" defaultValue="johny_01" />
              </FormGroup>
            </Col>
            <Col sm="12">
              <FormGroup>
                <Label for="name"> Nom</Label>
                <Input id="name" defaultValue="John" />
              </FormGroup>
            </Col>
            <Col sm="12">
              <FormGroup>
                <Label for="name"> Prénom</Label>
                <Input id="name" defaultValue="Akila" />
              </FormGroup>
            </Col>
            <Col sm="12">
              <FormGroup>
                <Label for="email">Email</Label>
                <Input id="email" defaultValue="john@akil.com" />
              </FormGroup>
            </Col>
            <Col sm="12">
              <Alert
                className="mb-2"
                color="warning"
                isOpen={this.state.visible}
                toggle={this.dismissAlert}
              >
                <p className="mb-0">
                 Votre Email n'a pas été confirmé. Vérifier vos emails s'il vous plait.
                  <span className="text-primary"> Renvoyer</span>
                </p>
              </Alert>
            </Col>
            
            <Col className="d-flex justify-content-start flex-wrap" sm="12">
              <Button.Ripple className="mr-50" type="submit" color="primary">
                Sauvegarder les changements
              </Button.Ripple>
              <Button.Ripple type="submit" color="danger">
               Supprimer
              </Button.Ripple>
            </Col>
          </Row>
        </Form>
        </div>
        </div>
        </div>
      </React.Fragment>
    )
  }
}
export default General
