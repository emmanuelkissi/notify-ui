import React from "react";

import {connect} from "react-redux"
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  CardImg
} from "reactstrap";
import { Package, Shield} from "react-feather";
import profileimg from "../../../../assets/img/elements/create-profil-1.jpg";
import {
 creatProfiles
} from "../../../@core/redux/actions/profile/index";
class New extends React.Component {
  state = {
    profilname: "test",
    profilpassword: "test",
    error:"",
   
  };
  handleInputChange = (e) => {
    e.preventDefault();
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    })
  }
  handlenewprofil = (e) => {
    e.preventDefault();
    this.props.creatProfiles(this.state)
  };
 
  render() {
    return (
      <div className={'mt-5'}>
        <Row className={'mr-1 ml-1'}>
          <Col lg={6} className={'mt-2'}>
            
            <Row>
            <CardImg top width="100%" src={profileimg} alt="Card image cap" />
            </Row>
          </Col>
          <Col lg={6} className={'mt-5'}>
            <div>
             
              <Card className={'mt-2'}>
                <CardHeader>
                  <CardTitle>Profil</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form >
                    <Row>
                      <Col sm="12" >
                        <Label for="nameVerticalIcons">Nom du profil</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="profilname"
                            id="nameVerticalIcons"
                            placeholder="Nom du profil"
                            onChange={this.handleInputChange}
                          />
                          <div className="form-control-position">
                            <Package size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      
                      <Col sm="12">
                        <Label for="IconsMobile">Mot de passe</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="password"
                            name="profilpassword"
                            id="IconsMobile"
                            placeholder="Mot de passe"
                            onChange={this.handleInputChange}
                          />
                          <div className="form-control-position">
                            <Shield size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <FormGroup className="has-icon-left position-relative">
                          <Button.Ripple
                            color="primary"
                            type="submit"
                            className="mr-1 mb-1"
                            onClick ={this.handlenewprofil}
                          >
                            Créer
                          </Button.Ripple>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    values: state.profile.profiles.data
  }
};

export default connect(mapStateToProps,
  {
    creatProfiles
  })(New)
