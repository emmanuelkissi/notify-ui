import React from "react";
import {connect} from "react-redux"

import {
  getProfiles,
  getProfileAccessToken
} from "../../../@core/redux/actions/profile/index";
import {Button, Card, CardBody, CardHeader, CardTitle} from "reactstrap";
import {ChevronRight} from "react-feather";
import img1 from "../../../../assets/img/portrait/small/avatar-s-5.jpg";
import {history} from "../../../../history";

const firstAl = (name) =>
{
  let info =name[0];
  return info.toUpperCase();
}
const ProfileCard = ({profileEntityName, selectProfile}) => {
  return (
    <div className="d-flex justify-content-between align-items-center mb-1">
      <div className="user-info d-flex align-items-center">
        <div className="icon-section" style={{margin: "0.5em"}}>
          <div
            className="avatar avatar-stats p-50 m-0 bg-rgba-info"
          >
            <div className="avatar-content"><span style={{fontSize: "1.8em", color: "#ff9f43"}}>{firstAl(profileEntityName)}</span></div>
          </div>
        </div>
        <div className="user-page-info">
          <h6 className="mb-0">{profileEntityName}</h6>
        </div>
      </div>
      <Button.Ripple color="primary" className="btn-icon ml-auto" onClick={selectProfile}>
        <ChevronRight size={17}/>
      </Button.Ripple>
    </div>
  );
}

class Profile extends React.Component {
  componentDidMount() {
    this.props.getProfiles()
   
  }
  newprofile =()=>{
    history.push('/dashboard/auth/profiles/new')
  }
  selectProfile = (e) => {
    e.preventDefault();
    this.props.getProfileAccessToken(this.props.profileSelected.id);
  }

  render() {
    return <div style={{width: '35em', margin: "10em auto"}}>
      <Card>
      <CardHeader>
         
            <div className="clearfix"  style={{ padding: '.5rem' }}>
            <CardTitle className="float-left">Choisir le Profile </CardTitle>
              <Button.Ripple  color="primary" className="btn-icon ml-4 float-right" onClick={ this.newprofile }>
                  Nouveau
              </Button.Ripple>
            </div>
          
      </CardHeader>
        <CardHeader>
          <CardTitle>{this.props.title}</CardTitle>
        </CardHeader>
        <CardBody>
          {this.props.profiles.map((profile, idx) => {
            return <ProfileCard
              key={idx}
              profileImg={img1}
              profileEntityName={profile.entity_name}
              selectProfile={this.selectProfile}
            />;
          })}
        </CardBody>
      </Card>
    </div>;
  }
}

const mapStateToProps = state => {
  return {
    profiles: state.profile.profiles,
    profileSelected: state.profile.profileSelected
  }
}

export default connect(mapStateToProps,
  {
    getProfiles,
    getProfileAccessToken
  })(Profile)
