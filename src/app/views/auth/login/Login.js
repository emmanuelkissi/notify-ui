import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label
} from "reactstrap"

import Checkbox from "../../../@vuexy/checkbox/CheckboxesVuexy"
import {Check, Mail, Lock} from "react-feather"
import {connect} from "react-redux"
import {loginWithJWT} from "../../../@core/redux/actions/auth/loginActions"

class Login extends React.Component {

  state = {
    email: "",
    password: "",
    error:"",
    
    remember: false
  };

  handleLogin = (e) => {
    e.preventDefault();
    this.props.loginWithJWT(this.state)
   
  };

  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>Connexion</CardTitle>
        </CardHeader>
        <CardBody>
          <Form onSubmit={this.handleLogin}>
            <Row>
              <Col sm="12">
                <Label for="EmailVerticalIcons">Email / Nom d'utilisateur</Label>
              <Label >  {this.state.error } </Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="email"
                    name="Email"
                    id="EmailVerticalIcons"
                    placeholder="Email / Nom d'utilisateur"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Mail size={15}/>
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsPassword">Mot de passe</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="password"
                    name="password"
                    id="IconsPassword"
                    placeholder="Mot de passe"
                    value={this.state.password}
                    onChange={e => this.setState({ password: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Lock size={15}/>
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Checkbox
                    color="primary"
                    icon={<Check className="vx-icon" size={16}/>}
                    label="Se souvenir de moi"
                    defaultChecked={false}
                  />
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Button.Ripple 
                    onClick={this.props.charge}
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                  >
                    Se connecter
                  </Button.Ripple>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    )
  }
}

const mapStateToProps = state => {
  return {
    values: state.auth.login
  }
};

export default connect(mapStateToProps, {loginWithJWT})(Login)
