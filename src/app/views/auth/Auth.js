import React from "react";
import {Nav, NavItem, NavLink, TabContent, TabPane, Row, Col,Alert} from "reactstrap";
import classnames from "classnames";
import Register from "./register/Register";
import Login from "./login/Login";
import {connect} from "react-redux";

const AlertFadeless = (props) => {
 
 
  return (
    <div>
      <Alert  color="danger" className="text-center"  isOpen={props.active}  fade={false}>
        {props.info}!
      </Alert>
      </div>
  );
}

class Auth extends React.Component {
  state = {
    active: "1",
    logininfo: "",
    loginactivealert: false,
    registerinfo: "",
    registeractivealert: false,
  };

  toggle = tab => {
    if (this.state.active !== tab) {
      this.setState({active: tab})
    }
  };
  
  loginchargeError = () =>{
    this.setState({
      logininfo: (this.props.info.error===undefined? "":  this.props.info.error),
      loginactivealert: (this.props.info.error===undefined ? false :true)

    }); 
  }
  registerchargeError = () =>{
    this.setState({
      registerinfo: (this.props.infor.error===undefined? "":  this.props.infor.error),
      registeractivealert: (this.props.infor.error===undefined ? false :true)

    }); 
  }
  dissError =() =>{
    this.setState({loginactivealert:false,registeractivealert:false})
  }
  logincharging =() =>{
    setTimeout(this.loginchargeError, 1000);
    setTimeout(this.dissError, 8000);
  }
  registercharging =() =>{
    setTimeout(this.registerchargeError, 1000);
    setTimeout(this.dissError, 8000);
  }
  render() {
    return (
        <div className={'mt-5'}>
          <Row>
              <Col style={{Display:"none"}}>
                  
                    <AlertFadeless info={this.state.logininfo} active={this.state.loginactivealert} >
                    
                    </AlertFadeless>
                    <AlertFadeless info={this.state.registerinfo} active={this.state.registeractivealert} >
                    
                    </AlertFadeless>
              </Col>
          </Row>
          <Row className={'mr-1 ml-1'}>
            <Col lg={7} className={'mt-2'}>
              <Row className="align-items-center">
                <Col lg={{ span: 3, offset: 4 }} md={6}>
                  <div className="title-heading mt-4">
                    <h1 className="heading mb-2">Notify</h1>
                    <p className="para-desc text-white" style={{fontSize:"18px"}}>Notify est un système autonome de push notificatif</p>
                    <p className="para-desc text-white" style={{fontSize:"15px"}}>Concentrez vous sur vos tâches et nous nous chargeons de tenir vos clients et utilisateurs 
                    informés en temps réel.</p>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col lg={4}>
              <Nav tabs className="justify-content-center mt-3">
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.active === "1"
                    })}
                    onClick={() => {
                      this.toggle("1")
                    }}
                  >
                    Connexion
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.active === "2"
                    })}
                    onClick={() => {
                      this.toggle("2")
                    }}
                  >
                    Inscription
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.active}>
                <TabPane tabId="1">
                  <Login charge={this.logincharging}/>
                </TabPane>
                <TabPane tabId="2">
                  <Register charge={this.registercharging}/>
                </TabPane>
              </TabContent>
            </Col>
          </Row>
        </div>
    )
  }
}

//const error = connect(mapStateToProps)(Auth);
//export  Auth;
const mapStateToProps = state => {
  return {
    info: state.auth.login,
    infor:state.auth.register
  }
};
export default connect(mapStateToProps)(Auth)