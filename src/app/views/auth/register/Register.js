import React from "react"
import {
  Card,
  CardHeader,
  CardTitle,
  CardBody,
  FormGroup,
  Row,
  Col,
  Input,
  Form,
  Button,
  Label
} from "reactstrap"

import {User, Mail, Smartphone, Lock } from "react-feather"
import {connect} from "react-redux"
import {registerWithJWT} from "../../../@core/redux/actions/auth/registerActions"
class Register extends React.Component {

  state = {
    first_name:"",
    last_name:"",
    email: "",
    mobile:"",
    fist_password: "",
    second_password: "",
    error:"",
    
    remember: false
  };

  handleRegister = (e) => {
    e.preventDefault();
    console.log("oklol")
    this.props.registerWithJWT(this.state)
   
  };
  render() {
    return (
      <Card>
        <CardHeader>
          <CardTitle>Inscription</CardTitle>
        </CardHeader>
        <CardBody>
          <Form onSubmit={this.handleRegister}>
            <Row>
              <Col sm="12">
                <Label for="nameVerticalIcons">Nom</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="text"
                    name="name"
                    id="nameVerticalIcons"
                    placeholder="Nom"
                    value={this.state.first_name}
                    onChange={e => this.setState({ first_name: e.target.value })}
                  />
                  <div className="form-control-position">
                    <User size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="nameVerticalIcons">Prenom</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="text"
                    name="name"
                    id="nameVerticalIcons"
                    placeholder="Prenom"
                    value={this.state.last_name}
                    onChange={e => this.setState({ last_name: e.target.value })}
                  />
                  <div className="form-control-position">
                    <User size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="EmailVerticalIcons">Email</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="email"
                    name="Email"
                    id="EmailVerticalIcons"
                    placeholder="Email"
                    value={this.state.email}
                    onChange={e => this.setState({ email: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Mail size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsMobile">Mobile</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="number"
                    name="mobile"
                    id="IconsMobile"
                    placeholder="Mobile"
                    value={this.state.mobile}
                    onChange={e => this.setState({ mobile: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Smartphone size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsPassword">Mot de passe</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="password"
                    name="password"
                    id="IconsPassword"
                    placeholder="Mot de passe"
                    value={this.state.first_password}
                    onChange={e => this.setState({ first_password: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Lock size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <Label for="IconsPassword">Confirmer mot de passe</Label>
                <FormGroup className="has-icon-left position-relative">
                  <Input
                    type="password"
                    name="password"
                    id="IconsPassword"
                    placeholder="Confirmer mot de passe"
                    value={this.state.second_password}
                    onChange={e => this.setState({ second_password: e.target.value })}
                  />
                  <div className="form-control-position">
                    <Lock size={15} />
                  </div>
                </FormGroup>
              </Col>
              <Col sm="12">
                <FormGroup className="has-icon-left position-relative">
                  <Button.Ripple
                    color="primary"
                    type="submit"
                    className="mr-1 mb-1"
                    onClick={this.props.charge}
                  >
                    S'inscrire
                  </Button.Ripple>
                </FormGroup>
              </Col>
            </Row>
          </Form>
        </CardBody>
      </Card>
    )
  }
}
const mapStateToProps = state => {
  return {
    values: state.auth.register
  }
};


export default connect(mapStateToProps, {registerWithJWT})(Register)


