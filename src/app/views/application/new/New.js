import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  CardImg
} from "reactstrap";
import { Package} from "react-feather";
import {connect} from "react-redux"
import {
  addData
 } from "../../../@core/redux/actions/data-list/index";
 import profileimg from "../../../../assets/img/elements/create-app-1.png";
class New extends React.Component {
  state = {
    app_name: "test",
   
    error:"",
   
  };
  handleInputChange = (e) => {
    e.preventDefault();
    const target = e.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    })
  }
  handlenewapp = (e) => {
    e.preventDefault();
    this.props.addData(this.state)
  };
  render() {
    return (
      <div className={'mt-5'}>
        <Row className={'mr-1 ml-1 mt-5'}>
          <Col lg={5} className={'mt-2'}>
          <Row>
            <CardImg top width="100%" src={profileimg} alt="Card image cap" />
            </Row>
          </Col>
          <Col lg={7} className={'m-auto'}>
            <div>
              <Card>
                <CardHeader>
                  <CardTitle>Application</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col sm="12">
                        <Label for="nameVerticalIcons">Nom de l'application</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="app_name"
                            id="nameVerticalIcons"
                            placeholder="Nom de l'application"
                            onChange={this.handleInputChange}
                          />
                          <div className="form-control-position">
                            <Package size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      
                      
                      
                      <Col sm="12">
                        <FormGroup className="has-icon-left position-relative">
                          <Button.Ripple
                            color="primary"
                            type="submit"
                            className="mr-1 mb-1"
                            onClick ={this.handlenewapp}
                          >
                            Créer
                          </Button.Ripple>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    values: null
  }
};

export default connect(mapStateToProps,
  {
    addData
  })(New)
