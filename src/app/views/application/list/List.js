import React from "react"
import {Row, Col, CardBody, Card, CardHeader, CardTitle,Form, FormGroup, Label, Input, Button} from "reactstrap"
import ListViewConfig from "../../../@vuexy/dataList/DataListConfig"
import queryString from "query-string"


class Application extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Row style={{marginTop: '9rem'}}>
          <Col sm="6">
           
            <Card> 
              <CardHeader>
                  <CardTitle>Votre Solde</CardTitle>
              </CardHeader>
              
              <CardBody>
              <CardTitle>15.000 </CardTitle>FCFA

              </CardBody>
            </Card>
          </Col>
          <Col sm="6">
          <Card> 
              <CardHeader>
                  <CardTitle>Faire un rechargement</CardTitle>
              </CardHeader>
              
              <CardBody>
                  <Form>
                    <FormGroup tag="fieldset">
                          
                          <FormGroup check>
                            <Label check>
                              <Input type="radio" name="radio1" />{' '}
                              Visa
                            </Label>
                          </FormGroup>
                          <FormGroup check>
                            <Label check>
                              <Input type="radio" name="radio1" />{' '}
                             Paypal
                            </Label>
                          </FormGroup>
                         
                          <FormGroup check>
                            <Label check>
                              <Input type="radio" name="radio1" />{' '}
                             MasterCard
                            </Label>
                          </FormGroup>
                          
                       </FormGroup>
                       <Button color="secondary">Recharger</Button>
                  </Form>
              </CardBody>
            </Card>
          </Col>
          <Col sm="12">
             <Card className="p-2  bg-secondary" >  
              <ListViewConfig
                parsedFilter={queryString.parse(this.props.location.search)}
              />
            </Card> 
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default Application
