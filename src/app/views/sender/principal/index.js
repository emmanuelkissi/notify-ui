import React  from "react";
import * as Icon from "react-feather";
//import {connect} from "react-redux"
import {Container, Col, Row, Card, CardTitle,Form, FormGroup, Label,CardText,InputGroup, InputGroupAddon, InputGroupText, Input} from "reactstrap";

import Editor from "../compo/editor"
class Sender extends React.Component {
state = {
    view_mail: false,
    view_web: false,
    view_sms: false,
    list_mail:[],
    list_sms:[],
    list_web:[],
    data_simple:null,
    data_complexe:null

}
    sender()
    { 
        let data ={
            autor : {
                view_mail: this.state.view_mail,
                view_web: this.state.view_web,
                view_sms: this.state.view_sms,
            },
            content :{
                data_simple:this.state.data_simple,
                data_complexe:this.state.data_complexe
            }
        }
       
        return data
    }
    render(){
       
        return (
            <Container>
                <Row>
                    <Col sm={{ size: 10,offset: 1 }}>
                      
                      <Card body>
                          <CardTitle>
                                Selection TypePush
                          </CardTitle>
                          <CardText>
                          <Row>
                          <Col sm={{ size: 6 , offset:3 }}>
                                     
                                <Form>
                                   
                               
                              
                                        <FormGroup check>
                                                <Label check>
                                                <Input type="checkbox" />{' '}
                                                SMS
                                                </Label>
                                        </FormGroup> 
                                        <FormGroup check>
                                                <Label check>
                                                <Input type="checkbox" />{' '}
                                                WEB
                                                </Label>
                                        </FormGroup>
                                        <FormGroup check>
                                                <Label check>
                                                <Input type="checkbox" />{' '}
                                                E-MAIL
                                                </Label>
                                        </FormGroup>
                                </Form> 
                                </Col>
                                </Row>
                          </CardText>    
                      </Card>
                      <Card body>
                          <CardTitle>
                                Ajout Destinataire(s)
                          </CardTitle>
                          <CardText>
                              <Row>
                            
                        <InputGroup >
                            <InputGroupAddon addonType="prepend">
                            <InputGroupText><Icon.Mail size={20}/></InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Email" />
                        </InputGroup>
                            <br/>
                        <InputGroup>
                            <InputGroupAddon addonType="prepend">
                            <InputGroupText><Icon.PhoneForwarded size={20}/></InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="Telephone" />
                        </InputGroup>
                            <br />
                            <InputGroup>
                            <InputGroupAddon addonType="prepend">
                            <InputGroupText><Icon.Link size={20}/></InputGroupText>
                            </InputGroupAddon>
                            <Input placeholder="lien de push web" />
                        </InputGroup>
                            <br />
                            </Row>
                          </CardText>
                           
                      </Card>
                    </Col>
                </Row>
                <Editor />
            </Container>
            
            
            )
        
    }
}

export default Sender;