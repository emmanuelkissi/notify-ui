
import React from 'react';
const firstAl = (name) =>
{
  let info =name[0];
  return info.toUpperCase();
}
const clientCard = (props) => {
    return (
      <div className="d-flex justify-content-between align-items-center mb-1">
        <div className="user-info d-flex align-items-center">
          <div className="icon-section" style={{margin: "0.5em"}}>
            <div
              className="avatar avatar-stats p-50 m-0 bg-rgba-info"
            >
              <div className="avatar-content"><span style={{fontSize: "1.8em", color: "#ff9f43"}}>{firstAl(props.name)}</span></div>
            </div>
          </div>
          <div className="user-page-info">
            <h6 className="mb-0">{props.name}</h6>
            <h6 className="mb-0">{props.email}</h6>
            <h6 className="mb-0">{props.tel}</h6>
          </div>
        </div>
       
      </div>
    );
  }


  export default clientCard;