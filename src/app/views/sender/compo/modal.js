/* eslint react/no-multi-comp: 0, react/prop-types: 0 */

import React, { useState } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

const firstAl = (name) =>
{
  let info =name[0];
  return info.toUpperCase();
}

const ModalClient= (props) => {
  const {
  
    className
  } = props;

  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <div>
      <Button color="danger" onClick={toggle}>{props.label}</Button>
      <Modal isOpen={modal} toggle={toggle} className={className}>
        <ModalHeader toggle={toggle}>{props.title}</ModalHeader>
        <ModalBody>
        <div className="d-flex justify-content-between align-items-center mb-1">
        <div className="user-info d-flex align-items-center">
          <div className="icon-section" style={{margin: "0.5em"}}>
            <div
              className="avatar avatar-stats p-50 m-0 bg-rgba-info"
            >
              <div className="avatar-content"><span style={{fontSize: "1.8em", color: "#ff9f43"}}>{firstAl("test")}</span></div>
            </div>
          </div>
          <div className="user-page-info ">
            <h6 className="mb-0">test </h6>
          
          </div>
          <div className="user-page-info ml-2">
            <h6 className="mb-0">test </h6>
          
          </div>
          <div className="user-page-info ml-2">
            <h6 className="mb-0">test </h6>
          
          </div>
        </div>
       
      </div>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>Terminer</Button>{' '}
          <Button color="secondary" onClick={toggle}>Annuler</Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default ModalClient;