import React, { useRef } from 'react';
import {Button} from "reactstrap";

import EmailEditor from 'react-email-editor';

const Editor = (props) => {
  const emailEditorRef = useRef(null);

  const exportHtml = () => {
    emailEditorRef.current.editor.exportHtml((data) => {
      const { /*design,*/ html } = data;
      console.log('exportHtml', html);
    });
  };

  const onLoad = () => {
    // you can load your template here;
    // const templateJson = {};
    // emailEditorRef.current.editor.loadDesign(templateJson);
  };

  return (
    <div>
      <div>
        <Button onClick={exportHtml}>Exporter un fichier html</Button>
      </div>
      
      <EmailEditor
        ref={emailEditorRef}
        onLoad={onLoad}
      />
    </div>
  );
};
export default Editor;