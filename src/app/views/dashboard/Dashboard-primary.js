import React from "react"
import {Row, Col, CardBody, Card, CardHeader, CardTitle, Table, Progress,Button} from "reactstrap"
import StatisticsCard from "../../@vuexy/statisticsCard/StatisticsCard"
import "../../../assets/scss/pages/dashboard-analytics.scss"
import {
  Crop,
   Award,Code,Users,Edit
} from "react-feather"
import {connect} from "react-redux"

import {
  getProfiles,
  getProfileAccessToken
} from "../../@core/redux/actions/profile/index";
import {history} from "../../../history";
import decorLeft from "../../../assets/img/elements/decore-left.png";
import decorRight from "../../../assets/img/elements/decore-right.png";

const WelcomeCard = () => {
  return <Card className="bg-analytics text-white sales-card">
    <CardBody className="text-center">
      <img src={decorLeft} alt="card-img-left" className="img-left"/>
      <img src={decorRight} alt="card-img-right" className="img-right"/>
      <div className="avatar avatar-xl bg-primary shadow avatar-dashboard mt-0">
        <div className="avatar-content">
          <Award className="text-white" size={28}/>
        </div>
      </div>
      <div className="award-info text-center">
        <h1 className="mb-2 text-white">Bienvenue Notify.</h1>
        <p className="m-auto mb-0 w-75">Nous sommes heureux de vous revoir.<br/> vous avez <strong>0 </strong>
          récuperations effectuées depuis votre dernière connexion.
        </p>
      </div>
    </CardBody>
  </Card>;
}


class Dashboard extends React.Component {
  componentDidMount() {
    this.props.getProfiles()
   
  }
   newprofile = () =>
  {
    history.push("/dashboard/auth/profiles/new")
  }
   selectprofile (id)
  {
   this.props.getProfileAccessToken(id);
    history.push("/dashboard/apps")
    
  }
 
  render() {
    return (
      <React.Fragment>
        <Row className="match-height">
          <Col lg="6" md="12">
            <WelcomeCard/>
          </Col>
          <Col xl="2" lg="6" sm="6">
            <StatisticsCard
              hideChart
              iconBg="primary"
              icon={<Users className="primary" size={22}/>}
              stat="0"
              statTitle="Nombre de profile"
            />
          </Col>
          <Col xl="2" lg="6" sm="6">
            <StatisticsCard
              hideChart
              iconBg="success"
              icon={<Code className="success" size={22}/>}
              stat="0"
              statTitle="Profile Crée"
            />
          </Col>
          <Col xl="2" lg="6" sm="6">
            <StatisticsCard
              hideChart
              iconBg="warning"
              icon={<Crop className="warning" size={22}/>}
              stat="0"
              statTitle="Profile associés"
            />
          </Col>
        </Row>
        <Row>
          <Col sm="12">
                                <Card>
                          <CardHeader>
                            <CardTitle>Gestion de profile</CardTitle>
                            <Button onClick={this.newprofile} color="primary">Nouveau Profile</Button>
                          </CardHeader>
                          <Table
                            responsive
                            className="dashboard-table table-hover-animation mb-0 mt-1"
                          >
                            <thead>
                            <tr>
                              <th>ID</th>
                              <th>NOM</th>
                              <th>STATUS</th>
                              <th>APPLICATIONS CONNECTEES</th>
                              <th>CREATION DE PROFILE</th>
                              <th>DATE DE CREATION</th>
                              <th>Type</th>
                              <th>OPTION</th>
                            
                            </tr>
                            </thead>
                            <tbody>
                            {this.props.profiles.map((taskPlanning, idx) => {
                              return <tr key={idx}>
                                <td>{taskPlanning.id}</td>
                                <td>{taskPlanning.entity_name}</td>
                                <td>
                                  <div
                                    className="bg-success"
                                    style={{
                                      height: "10px",
                                      width: "10px",
                                      borderRadius: "50%",
                                      display: "inline-block",
                                      marginRight: "5px"
                                    }}
                                  />
                                  <span>{taskPlanning.status}</span>
                                </td>
                                <td className="p-1">{taskPlanning.actors_nb}</td>
                                <td>
                                  <span>{taskPlanning.percentage} %</span>
                                  <Progress className="mb-0 mt-1" color="success" value={taskPlanning.percentage}/>
                                </td>
                                <td>{taskPlanning.start_date}</td>
                                <td>{taskPlanning.type}</td>
                                <td> <Edit className="primary" size={22} onClick={() => this.selectprofile(taskPlanning.id)}/> </td>
                              </tr>
                            })}
                            </tbody>
                          </Table>
                        </Card>;
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}
const mapStateToProps = state => {
  return {
    profiles: state.profile.profiles,
    profileSelected: state.profile.profileSelected
  }
}

  export default connect(mapStateToProps,
    {
      getProfiles,
      getProfileAccessToken
    })(Dashboard)
  