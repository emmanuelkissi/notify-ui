import React from "react"
import {Row, Col, Card, CardHeader, CardTitle, Table,CardText, CardBody,Badge} from "reactstrap"

import "../../../assets/scss/pages/dashboard-analytics.scss"




const RecentTaskPlanning = () => {
  const recentTaskPlanning = [
    {
      id: "#879985",
      status: "Envoyer",
      type: "SMS",
      date: "14:58 26/07/2018"
    }
  ];

  return <Card>
    <CardHeader>
      <CardTitle>Historiques</CardTitle>
    </CardHeader>
    <Table
      responsive
      className="dashboard-table table-hover-animation mb-0 mt-1"
    >
      <thead>
      <tr>
        <th>ID</th>
        <th>STATUS</th>
        <th>TYPE</th>
        <th>DATE </th>
        
      </tr>
      </thead>
      <tbody>
      {recentTaskPlanning.map((taskPlanning, idx) => {
        return <tr key={idx}>
          <td>{taskPlanning.id}</td>
          <td>
            <div
              className="bg-success"
              style={{
                height: "10px",
                width: "10px",
                borderRadius: "50%",
                display: "inline-block",
                marginRight: "5px"
              }}
            />
            <span>{taskPlanning.status}</span>
          </td>
          <td className="p-1">{taskPlanning.type}</td>
          
          <td>{taskPlanning.date}</td>
        
        </tr>
      })}
      </tbody>
    </Table>
  </Card>;
}

class Dashboard extends React.Component {
  render() {
    return (
      <React.Fragment>
        <Row className="match-height">
      
          <Col sm="12">
          <Card>
            <CardHeader>
              <CardTitle>Configuration API</CardTitle>
            </CardHeader>
            <CardBody>
                <CardText>
                  <Row>
                      <Col sm="6">
                      <Badge color="success">API Key</Badge> notify-0568-fj-581
                      </Col>
                      <Col sm="6">
                      <Badge color="warning">password</Badge> notify-pass-fj-581
                      </Col>
                  </Row>
                    
                </CardText>
               
            </CardBody>
          </Card>
          </Col>
        </Row>
        <Row>
          <Col sm="12">
            <RecentTaskPlanning/>
          </Col>
        </Row>
      </React.Fragment>
    )
  }
}

export default Dashboard
