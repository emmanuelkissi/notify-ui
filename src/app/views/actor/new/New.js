import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Form,
  FormGroup,
  Input,
  CustomInput,
  Label,
  Row,

} from "reactstrap";
import {Package, Lock, User} from "react-feather";
import Select from "react-select"

class New extends React.Component {
  state = {
    rowData: [],
  }

  async componentDidMount() {
    let rowData = [{ value: "nsia", label: "NSIA"}]
    this.setState({rowData})
  }

  render() {
    const {rowData} = this.state
    return (
      <div className={''}>

        <Row className="">
          <Col lg={12}>
            <div>
              <Card>
                <CardHeader>
                  <CardTitle>Compte bancaire</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col sm="12">
                        <Label for="rib">RIB</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="rib"
                            id="rib"
                            placeholder="RIB de compte bancaire"
                          />
                          <div className="form-control-position">
                            <Package size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="banque">Banque</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Select
                            className="React"
                            classNamePrefix="Selectionner une banque"
                            defaultValue={rowData['0']}
                            name="banque"
                            options={rowData}
                          />

                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="username">Identifiant</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="user"
                            id="username"
                            placeholder="Nom de banque"
                            required
                          />
                          <div className="form-control-position">
                            <User size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="pwd">Password</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="password"
                            id="pwd"
                            placeholder="Nom de banque"
                          />
                          <div className="form-control-position">
                            <Lock size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="data-status">Status</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <CustomInput
                            type="select"
                            id="data-status"
                            name="status"
                          >
                            <option>ouvert</option>
                            <option>fermé</option>

                          </CustomInput>

                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <FormGroup className="has-icon-left position-relative">
                          <Button.Ripple
                            color="primary"
                            type="submit"
                            className="mr-1 mb-1"
                          >
                            Créer
                          </Button.Ripple>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default New;
