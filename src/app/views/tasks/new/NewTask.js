import React from "react";
import axios from 'axios';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  CardTitle,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,

} from "reactstrap";
import {Package, Server, Clipboard} from "react-feather";



class New extends React.Component {
    state = {
        rowData:[],
     }

    async componentDidMount() {
        await axios.get("/api/datalist/banque").then(response => {
          let rowData = response.data
          this.setState({ rowData })
        })
      }

  render() {
    return (
      <div className={''} >

        <Row className="" >
          <Col lg={12}>
          <div>
              <Card>
                <CardHeader>
                  <CardTitle>Tâche</CardTitle>
                </CardHeader>
                <CardBody>
                  <Form>
                    <Row>
                      <Col sm="12">
                        <Label for="nameVerticalIcons">Banque</Label>
                        <FormGroup className="has-icon-left position-relative">
                        <Input
                         type="select"
                         id="data-status"
                         >
                         <option></option>
                         <option>SGBCI</option>
                         <option>NSIA</option>
                         <option>GT BANK</option>
                        </Input>
                          <div className="form-control-position">
                            <Package size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="nameVerticalIcons">RIB</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="text"
                            name="rib"
                            id="nameVerticalIcons"
                            placeholder="RIB de compte bancaire"
                            required
                          />
                          <div className="form-control-position">
                            <Server size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <Label for="dateop">Date d'operation</Label>
                        <FormGroup className="has-icon-left position-relative">
                          <Input
                            type="date"
                            name="date"
                            id="dateop"
                            placeholder="Url de redirection"
                            required
                          />
                          <div className="form-control-position">
                            <Clipboard size={15}/>
                          </div>
                        </FormGroup>
                      </Col>
                      <Col sm="12">
                        <FormGroup className="has-icon-left position-relative">
                          <Button.Ripple
                            color="primary"
                            type="submit"
                            className="mr-1 mb-1"
                            onClick={e => e.preventDefault()}
                          >
                            Créer
                          </Button.Ripple>
                        </FormGroup>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    )
  }
}

export default New;
