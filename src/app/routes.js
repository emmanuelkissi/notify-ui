import {lazy} from "react";

const ActorList = lazy(() =>
  import("./views/actor/list/List")
);
const NewActor = lazy(() =>
  import("./views/actor/new/New")
);

const TaskList = lazy(() =>
  import("./views/tasks/list/List")
);
const NewTask = lazy(() =>
  import("./views/tasks/new/NewTask")
);

const ScheduleList = lazy(() =>
  import("./views/schedule/list/List")
);
const NewSchedule = lazy(() =>
  import("./views/schedule/new/new")
);

const Profilenew = lazy(() =>
  import("./views/auth/profile/New")
);
const Profile = lazy(() =>
  import("./views/auth/profile/Profile")
);
const PersonnalProfile = lazy(() =>
  import("./views/auth/profile/General")
);
const Dashboard = lazy(() =>
  import("./views/dashboard/Dashboard")
);
const Dashprimary = lazy(() =>
  import("./views/dashboard/Dashboard-primary")
);
const AppList = lazy(() =>
  import("./views/application/list/List")
);
const newApp = lazy(() =>
  import("./views/application/new/New")
);
const Auth = lazy(() =>
  import("./views/auth/Auth")
);
const Sender = lazy(() =>
  import("./views/sender/principal/index")
);

const routes = [
  {
    path: "/dashboard/apps/:appId/schedules",
    component: ScheduleList,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  {
    path: "/dashboard/apps/:appId/newschedules",
    component: NewSchedule,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  //
  {
    path: "/dashboard/apps/:appId/tasks",
    component: TaskList,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  {
    path: "/dashboard/apps/:appId/newtasks",
    component: NewTask,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  //
  {
    path: "/dashboard/apps/:appId/actors",
    component: ActorList,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  {
    path: "/dashboard/apps/:appId/newactors",
    component: NewActor,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  {
    path: "/dashboard/apps/:appId/pusher",
    component: Sender,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  //
  {
    path: "/dashboard/apps/:appId",
    component: Dashboard,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  {
    path: "/dashboard/apps",
    component: AppList,
    EntityLayout:true,
    //withoutMenuLayout: true,
    profileSelectIsRequired: true,
    userAuthenticationIsRequired: true
  },
  {
    path: "/dashboard/new/apps",
    component: newApp,
   // fullLayout: true,
    //withoutMenuLayout: true,
    EntityLayout:true,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: true
  },
  {
    path: "/dashboard/auth/profiles/new",
    component: Profilenew,
   // fullLayout: true,
   // withoutMenuLayout: true,
    EntityLayout:true,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: null
  },
  {
    path: "/dashboard/auth/profiles",
    component: Profile,
   // fullLayout: true,
    //withoutMenuLayout: true,
    EntityLayout:true,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: null
  }
  ,
  
  {
    path: "/dashboard/principal/user-edit",
    component: PersonnalProfile,
    EntityLayout:true,
   // fullLayout: true,
   // withoutMenuLayout: true,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: null
  },
  {
    path: "/dashboard/auth",
    component: Auth,
    fullLayout: true,
    userAuthenticationIsRequired: false,
    profileSelectIsRequired: false
  },
  {
    path: "/dashboard",
    component: Dashprimary,
    
   // fullLayout: true,
    //withoutMenuLayout: true,
    EntityLayout:true,
    userAuthenticationIsRequired: true,
    profileSelectIsRequired: null
  }
  ,
  {
    path: "/dashboard/sender",
    component: Sender,
    fullLayout: true,
    userAuthenticationIsRequired: false,
    profileSelectIsRequired: false
  }
]

export default routes;
