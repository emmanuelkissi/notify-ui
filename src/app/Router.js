import React, {Suspense} from "react";
import {Router, Switch, Route, Redirect} from "react-router-dom";
import {history} from "../history";
import {connect} from "react-redux";
import Spinner from "./@vuexy/spinner/Loading-spinner";
import {ContextLayout} from "./@core/utility/context/Layout";
import routes from "./routes";

const RouteConfig = (
  {
    component: Component,
    fullLayout,
    landingLayout,
    withoutMenuLayout,
    EntityLayout,
    permission,
    isNotSecure,
    user,
    userAuthenticationIsRequired,
    profileSelectIsRequired,
    isUserAuthenticated,
    isProfileSelected,
    ...rest
  }) => {

  const isNotSecured = userAuthenticationIsRequired === false
    && isUserAuthenticated === false;

  const isSecuredByUser = userAuthenticationIsRequired
    && userAuthenticationIsRequired === isUserAuthenticated
    && profileSelectIsRequired === false;

  const isSecuredByProfile = userAuthenticationIsRequired
    && userAuthenticationIsRequired === isUserAuthenticated
    && profileSelectIsRequired === true
    && profileSelectIsRequired === isProfileSelected
 const isNecessaryonlyAuthentification = userAuthenticationIsRequired
  && userAuthenticationIsRequired === isUserAuthenticated
   // && userAuthenticationIsRequired === isUserAuthenticated
    && profileSelectIsRequired === null
  return (
    <Route
      {...rest}
      render={props => {
        return (
          isNotSecured || isSecuredByUser ||  isSecuredByProfile || isNecessaryonlyAuthentification?
            (<ContextLayout.Consumer>
              {context => {
                let LayoutTag =
                  fullLayout === true
                    ? context.fullLayout 
                    : EntityLayout=== true
                    ?context.EntityLayout
                    : context.state.activeLayout === "horizontal"
                    ? context.horizontalLayout
                    : context.VerticalLayout;
                LayoutTag = withoutMenuLayout ? context.withoutMenuLayout : LayoutTag;
                return (
                  <LayoutTag {...props} permission={props.user}>
                    <Suspense fallback={<Spinner/>}>
                      <Component {...props} />
                    </Suspense>
                  </LayoutTag>
                )
              }}
            </ContextLayout.Consumer>)
            : (
              !isUserAuthenticated  ?
                <Redirect to={{pathname: '/dashboard/auth'}}/>
                : <Redirect to={{pathname: '/dashboard'}}/>
            )
        )
      }}
    />
  )
    ;
};

const mapStateToProps = state => {
  return {
    user: state.auth,
    isUserAuthenticated: state.auth.login.isAuthenticated!== false || state.auth.register.isAuthenticated !== false ?   true : false,
    isProfileSelected: state.profile.profileIsSelected
  }
};

const AppRoute = connect(mapStateToProps)(RouteConfig);

class AppRouter extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          {
            routes.map((route, idx) => {
              return (
                <AppRoute
                  {...route}
                  key={idx}
                />);
            })
          }
        </Switch>
      </Router>
    )
  }
}

export default AppRouter
