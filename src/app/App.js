import React from "react"
import Router from "./Router"
import "./@vuexy/rippleButton/RippleButton"

import "react-perfect-scrollbar/dist/css/styles.css"
import "prismjs/themes/prism-tomorrow.css"
import {Provider} from "react-redux";
import {store} from "./@core/redux/storeConfig/store";
import "./@core/@fake-db";

const App = () => {
  return (
    <Provider store={store}>
      <Router/>
    </Provider>
  )
}

export default App
