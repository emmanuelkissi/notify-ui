import { history } from "../../../../../history"


import axios from "axios"


export const registerWithJWT = user => {

  return dispatch => {
    axios
      .post("/api/authenticate/register/user", {
        
        first_name:user.first_name,
        last_name:user.last_name,
        email: user.email,
        mobile: user.mobile,
        first_password: user.first_password,
        second_password: user.second_password,
      })
      .then(response => {
        var loggedInUser
   
        if(response.data){
          console.log(response.data)
          if(response.data.errors)
          {
            
            
              dispatch({
                type: "SIGNUP_WITH_JWT",
                payload: { error: response.data.errors , auth:false}
              })
            
          }else{
          loggedInUser = response.data.user

          localStorage.setItem("token", response.data.token)

          dispatch({
            type: "SIGNUP_WITH_JWT",
            payload: { loggedInUser, loggedInWith: "jwt" ,auth:true}
          })

          history.push("/dashboard/")
        }
        }

      })
      .catch(err => console.log(err))

  }

}
