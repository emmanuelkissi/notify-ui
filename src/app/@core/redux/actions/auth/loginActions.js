import {history} from "../../../../../history"
import axios from "axios"


export const loginWithJWT = user => {
  return dispatch => {
    axios
      .post("/api/authenticate/login/user", {
        email: user.email,
        password: user.password
      })
      .then(response => {
        let loggedInUser;
        console.log(response.data.error);
      
        if (response.data.error===undefined) {
          
          loggedInUser = response.data.user;
          localStorage.setItem('access_token', response.data.accessToken);
          dispatch({
            type: "LOGIN_WITH_JWT",
            isAuthenticated: true,
            payload: {loggedInUser:loggedInUser, loggedInWith: "jwt",auth:true},
          });
          history.push("/dashboard")
        }else{
          console.log("error")
          dispatch({
            type: "LOGIN_WITH_JWT",
            payload: {error:response.data.error, auth:false},
          });
        }
      })
      .catch(err => console.log(err))
  }
};

export const logoutWithJWT = () => {
  return dispatch => {
    dispatch({type: "LOGOUT_WITH_JWT", payload: {}});
    dispatch({type: "LOGOUT_PROFILE", payload: {}});
    localStorage.clear();
    history.push("/dashboard/auth");
  }
};
