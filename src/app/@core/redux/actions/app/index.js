import axios from "axios";

export const getApplications = () => {
  return async dispatch => {
    await axios.get("/api/datalist/initial-data").then(response => {
      dispatch({ type: "GET_APP", data: response.data })
    })
  }
}
