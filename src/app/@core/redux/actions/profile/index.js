import axios from "axios";
import {history} from "../../../../../history";


export const getProfiles = () => {
  console.log("active")
  return async dispatch => {
    await axios
      .get('/api/profiles')
      .then((res) => {
        dispatch({
          type: 'GET_PROFILES',
          data: res.data.data
        })
       
      })
  }
}
export const creatProfiles = (profil) => {
  return async dispatch => {
    await axios
     
      .post("/api/profiles/new", {
        profilname: profil.profilname,
        profilpassword: profil.profilpassword
      })
      .then((res) => {
        console.log(res.data)
        if(res.data==="save"){
         
          history.push('/dashboard')
        }else{
          dispatch({
            type: 'CREAT_PROFILES',
            data: "error"
          })
        }
        
       
      })
  }
}

export const getProfileAccessToken = (profileId) => {
  profileId = 1;
  return async dispatch => {
    await axios
      .get(`/api/profiles/${profileId}/token`)
      .then((res) => {
        localStorage.setItem('p_selected_access_token', res.data.data.access_token);
        dispatch({
          type: 'GET_PROFILE_ACCESS_TOKEN',
          data: res.data.data
        })
        history.push('/dashboard/apps')
      })
  }
}
