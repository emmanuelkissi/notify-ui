import {combineReducers} from "redux"
import customizer from "./customizer/"
import auth from "./auth/"
import navbar from "./navbar/index"
import dataList from "./data-list/"
import AppReducer from "./app/index";
import ProfileReducer from "./profile";

const rootReducer = combineReducers({
  customizer: customizer,
  auth: auth,
  navbar: navbar,
  dataList: dataList,
  app: AppReducer,
  profile: ProfileReducer
});

export default rootReducer
