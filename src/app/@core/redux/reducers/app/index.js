const initialState = {
  data: []
}

const AppReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_APP': {
      return {
        ...state,
        data: action.data
      }
    }
    default:
      return state
  }
}

export default AppReducer;
