const initialState = {
  profiles: [],
  profileSelected: {},
  profileIsSelected: !!localStorage.getItem('p_selected_access_token')
}

const ProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_PROFILES':
      console.log("ok cherche profil")
      return {
        ...state,
        profiles: action.data
      }
    case 'CREAT_PROFILES':
        return {
          ...state,
          profiles: action.data
        }
    case 'GET_PROFILE_ACCESS_TOKEN':
      return {
        ...state,
        profileSelected: action.data,
        profileIsSelected: true
      }
    case 'LOGOUT_PROFILE':
      return {
        ...state,
        profileSelected: action.data,
        profileIsSelected: false
      }
    default:
      return {
        ...state
      }
  }
}

export default ProfileReducer;
