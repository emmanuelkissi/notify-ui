
export const register = (state = {
  userRole: "admin",
  isAuthenticated: false
}, action) => {
  switch (action.type) {
    case "SIGNUP_WITH_EMAIL": {
      return { ...state, values: action.payload }
    }
    case "SIGNUP_WITH_JWT":
      return {
        ...state,isAuthenticated: action.payload.auth,
        values: action.payload, error:(action.payload.error===undefined? "":action.payload.error)
      }
    default: {
      return state
    }
  }
}
