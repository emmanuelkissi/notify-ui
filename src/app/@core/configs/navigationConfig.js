import React from "react"
import * as Icon from "react-feather"

const navigationConfig = (appId) => {
  return [
    {
      id: "dashboard",
      title: "Tableau de bords",
      type: "item",
      icon: <Icon.Home size={20}/>,
      navLink: `/dashboard/apps/${appId}`
    },
    {
      id: "actor",
      title: "Comptes connectés",
      type: "item",
      icon: <Icon.Database size={20}/>,
      badge: "primary",
      badgeText: "new",
      permissions: ["admin", "editor"],
      navLink: `/dashboard/apps/${appId}/actors`
    },
    /*{
      id: "user",
      title: "Mes Clients",
      type: "item",
      icon: <Icon.User size={20}/>,
      permissions: ["admin", "editor"],
      navLink: `/dashboard/apps/${appId}/clients`
    },*/
    {
      id: "pusher",
      title: "Pusher",
      type: "item",
      icon: <Icon.Send size={20}/>,
      permissions: ["admin", "editor"],
      navLink: `/dashboard/apps/${appId}/pusher`
    },
    /*{
      id: "schedule",
      title: "Planification Push",
      type: "item",
      icon: <Icon.Calendar size={20}/>,
      permissions: ["admin", "editor"],
      navLink: `/dashboard/apps/${appId}/schedules`,

    },
    {
      id: "developer",
      title: "Développeurs",
      type: "item",
      icon: <Icon.Terminal size={20}/>,
      permissions: ["admin", "editor"],
      navLink: `/dashboard/apps/${appId}/developer`,
      disabled: false
    },*/
    {
      id: "setting",
      title: "Paramètres",
      type: "item",
      icon: <Icon.Settings size={20}/>,
      permissions: ["admin", "editor"],
      navLink: `/dashboard/apps/${appId}/setting`,
      disabled: false
    }
  ]
}

export default navigationConfig
