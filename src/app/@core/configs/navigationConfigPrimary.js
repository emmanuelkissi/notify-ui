import React from "react"
import * as Icon from "react-feather"
const navigationConfig = [
  {
    id: "dashboard",
    title: "Tableau de bords",
    type: "item",
    icon: <Icon.Home size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard"
  },
 
  {
    id: "conf",
    title: "configuration",
    type: "item",
    icon: <Icon.Settings size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/settings",
    
  },
 
  {
    id: "Documentation",
    title: "Documentation",
    type: "item",
    icon: <Icon.Coffee size={20} />,
    permissions: ["admin", "editor"],
    navLink: "/dashboard/documentation",
   
  }
];

export default navigationConfig
