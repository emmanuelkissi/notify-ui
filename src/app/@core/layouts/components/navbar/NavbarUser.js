import React from "react"
import {
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Media,
  Badge
} from "reactstrap"
import PerfectScrollbar from "react-perfect-scrollbar"
import axios from "axios"
import * as Icon from "react-feather"
import classNames from "classnames"
import Autocomplete from "../../../../@vuexy/autoComplete/AutoCompleteComponent"
import {history} from "../../../../../history"
import {connect} from "react-redux"
//import {loadSuggestions,updateStarred} from "../../../redux/actions/navbar/"
import {logoutWithJWT} from "../../../redux/actions/auth/loginActions"
const handleNavigation = (e, path) => {
  e.preventDefault()
  history.push(path)
}

/*const suggestion =[
  {
    id: 1,
    target: "Profile list",
    title: "Profile list",
    link: "/dashboard/auth/profiles",
    icon: "Home",
    starred: false
  }
]*/

const notifications = [
  {
    icon: <Icon.PlusSquare
      className="font-medium-5 primary"
      size={21}
    />,
    type: "primary",
    msgTitle: "Création du compte",
    msgText: "Avez vous ajoutez un nouveau compte",
    delay: "Depuis 9h"
  },
  {
    icon: <Icon.AlertTriangle
      className="font-medium-5 danger"
      size={21}
    />,
    type: "danger",
    msgTitle: "Facturation",
    msgText: "Vous avez une facture en attente",
    delay: "Depuis 9h"
  },
  {
    icon: <Icon.CheckCircle
      className="font-medium-5 info"
      size={21}
    />,
    type: "info",
    msgTitle: "Récuperation programmé",
    msgText: "La recuperation programmé a été achevé avec succes.",
    delay: "Depuis 9h"
  }
]

const NavBarNotificationUser = () => {
  return <UncontrolledDropdown
    tag="li"
    className="dropdown-notification nav-item"
  >
    <DropdownToggle tag="a" className="nav-link nav-link-label">
      <Icon.Bell size={21}/>
      <Badge pill color="primary" className="badge-up">
        {" "}
        {notifications.length}{" "}
      </Badge>
    </DropdownToggle>
    <DropdownMenu tag="ul" right className="dropdown-menu-media">
      <li className="dropdown-menu-header">
        <div className="dropdown-header mt-0">
          <h3 className="text-white">{notifications.length}</h3>
          <span className="notification-title">Notifications</span>
        </div>
      </li>
      <PerfectScrollbar
        className="media-list overflow-hidden position-relative"
        options={{
          wheelPropagation: false
        }}
      >
        {notifications.map((notification) => {
          return (
            <div className="d-flex justify-content-between">
              <Media className="d-flex align-items-start">
                <Media left href="#">
                  {notification.icon}
                </Media>
                <Media body>
                  <Media heading className={`${notification.type} media-heading`} tag="h6">
                    {notification.msgTitle}
                  </Media>
                  <p className="notification-text">
                    {notification.msgText}
                  </p>
                </Media>
                <small>
                  <time
                    className="media-meta"
                    dateTime="2015-06-11T18:29:20+08:00"
                  >
                    {notification.delay}
                  </time>
                </small>
              </Media>
            </div>
          );
        })}
      </PerfectScrollbar>
      <li className="dropdown-menu-footer">
        <DropdownItem tag="a" className="p-1 text-center">
          <span className="align-middle">Lire toutes les notifications</span>
        </DropdownItem>
      </li>
    </DropdownMenu>
  </UncontrolledDropdown>;
}

const NavBarUserDetail = ({profiles, userName,userEmail, userImg, logoutWithJWT}) => {
  /*const renderMails = profiles.map((profile, idx) => {
    return (
      <div
        key={idx}
        className="d-flex justify-content-between">
        <Media className="d-flex align-items-start">
          <Media left href="#">
                 <span data-tour="user">
                 <img
                   src={profile.img}
                   className="round"
                   height="40"
                   width="40"
                   alt="avatar"
                 />
                 </span>
          </Media>
          <Media body>
            <Media heading className="media-heading" tag="h6">
              {profile.name}
            </Media>
          </Media>
          <small>
            <time
              className="media-meta"
              dateTime="2015-06-11T18:29:20+08:00"
            >
              {profile.time}
            </time>
          </small>
        </Media>
      </div>
    )
  })
  <PerfectScrollbar
          className="media-list overflow-hidden position-relative"
          options={{
            wheelPropagation: false
          }}
        >
          {renderMails}
        </PerfectScrollbar> 
  
  
  */

  return (
    <UncontrolledDropdown tag="li" className="dropdown-user nav-item">
      <DropdownToggle tag="a" className="nav-link dropdown-user-link">
        <div className="user-nav d-sm-flex d-none">
        <span className="user-name text-bold-600">
          {userName}
        </span>
          <span className="user-status">Actif</span>
        </div>
        <span data-tour="user">
        <img
          src={userImg}
          className="round"
          height="40"
          width="40"
          alt="avatar"
        />
      </span>
      </DropdownToggle>
      <DropdownMenu tag="ul" right className="dropdown-menu-media">
        <li className="dropdown-menu-header">
          <div className="dropdown-header mt-0">
            <h3 className="">{userName}</h3>
            <span className="notification-title">{userEmail}</span>
          </div>
        </li>
        <NavBarUserAction
          actionTitle="Historique des connexions"
          action={(e) => {
            handleNavigation(e, "/dashboard/apps/1/user-edit")
          }}
          icon={<Icon.User size={20} className="mr-50"/>}
        />
         <DropdownItem divider/>
        <NavBarUserAction
          actionTitle="Mes Profils"
          action={(e) => {
            handleNavigation(e, "/dashboard/auth/profiles")
          }}
          icon={<Icon.User size={20} className="mr-50"/>}
        />
       
        <NavBarUserAction
          actionTitle="Editer"
          action={(e) => {
            handleNavigation(e, "/dashboard/principal/user-edit")
          }}
          icon={<Icon.User size={20} className="mr-50"/>}
        />
        <DropdownItem divider/>
        <NavBarUserAction
          actionTitle="Se deconnecter"
          action={() => logoutWithJWT()}
          icon={<Icon.Power size={20} className="mr-50"/>}
        />
        <DropdownItem divider/>
      </DropdownMenu>
    </UncontrolledDropdown>
  );

}

const NavBarUserAction = ({icon, actionTitle, action}) => {
  return <DropdownItem
    tag="a"
    href="#"
    onClick={action}
  >
    {icon}
    <span className="align-middle">{actionTitle}</span>
  </DropdownItem>;
}

class NavbarUser extends React.PureComponent {
  state = {
    navbarSearch: false,
    suggestions: [],
    data: [],
    rowData: [],
    value: "",
    totalPages: 0,
    currentPage: 0,
  }

  handleNavbarSearch = () => {
    this.setState({
      navbarSearch: !this.state.navbarSearch
    })
  }

  async componentDidMount() {
    await axios.get("/api/datalist/profil").then(response => {
      let rowData = response.data
      this.setState({rowData})
    })
  }

  render() {
    const {rowData} = this.state

    return (
      <ul className="nav navbar-nav navbar-nav-user float-right">
        <NavItem
          className="nav-search"
          onClick={this.handleNavbarSearch}
        >
          <NavLink className="nav-link-search">
            <Icon.Search size={21} data-tour="search"/>
          </NavLink>
          <div
            className={classNames("search-input", {
              open: this.state.navbarSearch,
              "d-none": this.state.navbarSearch === false
            })}
          >
            <div className="search-input-icon">
              <Icon.Search size={17} className="primary"/>
            </div>
            <Autocomplete
              className="form-control"
              suggestions={this.state.suggestions}
              filterKey="title"
              filterHeaderKey="groupTitle"
              grouped={true}
              placeholder="Rechercher..."
              autoFocus={true}
              clearInput={this.state.navbarSearch}
              externalClick={() => {
                this.setState({navbarSearch: false})
              }}
              onKeyDown={e => {
                if (e.keyCode === 27 || e.keyCode === 13) {
                  this.setState({
                    navbarSearch: false
                  })
                  this.props.handleAppOverlay("")
                }
              }}
              customRender={(
                item,
                i,
                filteredData,
                activeSuggestion,
                onSuggestionItemClick,
                onSuggestionItemHover
              ) => {
                const IconTag = Icon[item.icon ? item.icon : "X"]
                return (
                  <li
                    className={classNames("suggestion-item", {
                      active: filteredData.indexOf(item) === activeSuggestion
                    })}
                    key={i}
                    onClick={e => onSuggestionItemClick(item.link, e)}
                    onMouseEnter={() =>
                      onSuggestionItemHover(filteredData.indexOf(item))
                    }
                  >
                    <div
                      className={classNames({
                        "d-flex justify-content-between align-items-center":
                          item.file || item.img
                      })}
                    >
                      <div className="item-container d-flex">
                        {item.icon ? (
                          <IconTag size={17}/>
                        ) : item.file ? (
                          <img
                            src={item.file}
                            height="36"
                            width="28"
                            alt={item.title}
                          />
                        ) : item.img ? (
                          <img
                            className="rounded-circle mt-25"
                            src={item.img}
                            height="28"
                            width="28"
                            alt={item.title}
                          />
                        ) : null}
                        <div className="item-info ml-1">
                          <p className="align-middle mb-0">{item.title}</p>
                          {item.by || item.email ? (
                            <small className="text-muted">
                              {item.by
                                ? item.by
                                : item.email
                                  ? item.email
                                  : null}
                            </small>
                          ) : null}
                        </div>
                      </div>
                      {item.size || item.date ? (
                        <div className="meta-container">
                          <small className="text-muted">
                            {item.size
                              ? item.size
                              : item.date
                                ? item.date
                                : null}
                          </small>
                        </div>
                      ) : null}
                    </div>
                  </li>
                )
              }}
              onSuggestionsShown={userInput => {
                if (this.state.navbarSearch) {
                  this.props.handleAppOverlay(userInput)
                }
              }}
            />
            <div className="search-input-close">
              <Icon.X
                size={24}
                onClick={(e) => {
                  e.stopPropagation()
                  this.setState({
                    navbarSearch: false
                  })
                  this.props.handleAppOverlay("")
                }}
              />
            </div>
          </div>
        </NavItem>
        <NavBarNotificationUser/>
        <NavBarUserDetail
          profiles={rowData}
          userName={this.props.user.values.loggedInUser.last_name}
          userEmail={this.props.user.values.loggedInUser.email}
          userImg={this.props.userImg}
          logoutWithJWT={this.props.logoutWithJWT}
        />
      </ul>
    )
  }
}

//export default NavbarUser
const mapStateToProps = state => {
  return {
    user: state.auth.login,
  }
};

export default connect(
  mapStateToProps,
  {logoutWithJWT})
(NavbarUser)
