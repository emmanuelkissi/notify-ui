import React, {Component} from "react"
import {NavLink} from "react-router-dom"

class SidebarHeader extends Component {
  render() {
    return (
      <div className="navbar-header">
        <ul className="nav navbar-nav flex-row">
          <li className="nav-item mr-auto">
            <NavLink to="/" className="navbar-brand">
              <h2 className="brand-text mb-0">Notify.</h2>
            </NavLink>
          </li>
        </ul>
      </div>
    )
  }
}

export default SidebarHeader
