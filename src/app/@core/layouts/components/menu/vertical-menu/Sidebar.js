import React, {Component, useState} from "react"
import classNames from "classnames"
import {ContextLayout} from "../../../../utility/context/Layout"
import {connect} from "react-redux"
import SidebarHeader from "./SidebarHeader"
import Hammer from "react-hammerjs"
import SideMenuContent from "./sidemenu/SideMenuContent"
import PerfectScrollbar from "react-perfect-scrollbar"
import {Row, Col, NavLink, UncontrolledTooltip, NavItem} from "reactstrap"
import {history} from "../../../../../../history";
import Avatar from "../../../../../@vuexy/avatar/AvatarComponent";

import {
  getApplications,
} from "../../../../redux/actions/app/index"

const SubscribedAppSideBarItem = ({appName, appId, appColor, appNbNotification}) => {
  return (
    <NavItem
      style={{"padding": "0", "margin": "0 auto"}}
      className="nav-item d-lg-block"
    >
      <NavLink id={appId} onClick={() => history.push(`/dashboard/apps/${appId}`)}>
        <Avatar
          className="mr-1"
          color={appColor}
          badgeText={appNbNotification}
          badgeColor="primary"
          badgeUp
        >{appName.charAt(0)}
        </Avatar>
      </NavLink>
      <UncontrolledTooltip
        placement="top"
        target={appId}>{appName}
      </UncontrolledTooltip>
    </NavItem>
  );
}

const SubscribedAppsSideBar = ({width, sidebarState, visibilityState, applications}) => {
  const SubscribedAppSideBarItems = applications.data.map(({name, slug, color, nb_notifications}, idx) => {
    return <SubscribedAppSideBarItem
      key={idx}
      appName={name}
      appId={slug}
      appColor={color}
      appNbNotification={nb_notifications}
    />
  })

  return (
    <div className={classNames(`app-menu menu-fixed menu-light`, {
        collapsed: sidebarState === true,
        "hide-sidebar":
          width < 1200 && visibilityState === false
      }
    )}>
      {SubscribedAppSideBarItems}
    </div>
  );
}

const MainAppSideBar = (
  {
    appId,
    activeTheme,
    toggle,
    color,
    dir,
    sidebarWidth,
    sidebarState,
    sidebarVisibilityState,
    sidebarVisibility,
    sidebarHover,
    toggleSidebarMenu,
    setActiveIndex,
    sidebarCollapsed,
    sidebarMenuShadow,
    activePath,
    ScrollbarTag,
    scrollShadow,
    hoveredMenuItem,
    activeIndex,
    activeItem,
    currentLang,
    permission,
    currentUser,
    collapsedMenuPaths,
    deviceWidth,
    handleSidebarMouseEnter,
    handleActiveItem
  }
) => {
  const [menuShadow, setMenuShadow] = useState(sidebarMenuShadow)
  return (
    <div className={classNames(`main-menu menu-fixed menu-light menu-accordion menu-shadow theme-${activeTheme}`, {
        collapsed: sidebarState === true,
        "hide-sidebar": sidebarWidth < 1200 && sidebarVisibilityState === false
      }
    )} onMouseEnter={() => sidebarHover(false)}
         onMouseLeave={() => sidebarHover(true)}
    >
      <SidebarHeader
        toggleSidebarMenu={toggleSidebarMenu}
        toggle={toggle}
        sidebarBgColor={color}
        sidebarVisibility={sidebarVisibility}
        activeTheme={activeTheme}
        collapsed={sidebarCollapsed}
        menuShadow={menuShadow}
        activePath={activePath}
        sidebarState={sidebarState}
      />
      <ScrollbarTag
        className={classNames("main-menu-content", {
          "overflow-hidden": ScrollbarTag !== "div",
          "overflow-scroll": ScrollbarTag === "div"
        })}
        {...(ScrollbarTag !== "div" && {
          options: {wheelPropagation: false},
          onScrollDown: container => scrollShadow(container, "down"),
          onScrollUp: container => scrollShadow(container, "up"),
          onYReachStart: () =>
            menuShadow === true &&
            setMenuShadow()
        })}>
        <Hammer
          onSwipe={() => {
            sidebarVisibility()
          }}
          direction={
            dir === "rtl" ? "DIRECTION_RIGHT" : "DIRECTION_LEFT"
          }>
          <ul className="navigation navigation-main">
            <SideMenuContent
              appId={appId}
              setActiveIndex={setActiveIndex}
              activeIndex={activeIndex}
              hoverIndex={hoveredMenuItem}
              handleSidebarMouseEnter={handleSidebarMouseEnter}
              activeItemState={activeItem}
              handleActiveItem={handleActiveItem}
              activePath={activePath}
              lang={currentLang}
              permission={permission}
              currentUser={currentUser}
              collapsedMenuPaths={collapsedMenuPaths}
              toggleMenu={sidebarVisibility}
              deviceWidth={deviceWidth}
            />
          </ul>
        </Hammer>
      </ScrollbarTag>
    </div>);
};


class Sidebar extends Component {
  static getDerivedStateFromProps(props, state) {
    if (props.activePath !== state.activeItem) {
      return {
        activeItem: props.activePath
      }
    }
    return null
  }

  state = {
    width: window.innerWidth,
    activeIndex: null,
    hoveredMenuItem: null,
    activeItem: this.props.activePath,
    menuShadow: false,
    ScrollbarTag: PerfectScrollbar
  }

  mounted = false

  updateWidth = () => {
    if (this.mounted) {
      this.setState(prevState => ({
        width: window.innerWidth
      }))
      this.checkDevice()
    }
  }

  componentDidMount() {
    this.mounted = true
    if (this.mounted) {
      if (window !== "undefined") {
        window.addEventListener("resize", this.updateWidth, false)
      }
      this.checkDevice()
    }
    this.props.getApplications()
  }

  componentWillUnmount() {
    this.mounted = false
  }

  checkDevice = () => {
    var prefixes = " -webkit- -moz- -o- -ms- ".split(" ")
    var mq = function (query) {
      return window.matchMedia(query).matches
    }

    if ("ontouchstart" in window || window.DocumentTouch) {
      this.setState({
        ScrollbarTag: "div"
      })
    } else {
      this.setState({
        ScrollbarTag: PerfectScrollbar
      })
    }
    var query = ["(", prefixes.join("touch-enabled),("), "heartz", ")"].join("")
    return mq(query)
  }

  changeActiveIndex = id => {
    if (id !== this.state.activeIndex) {
      this.setState({
        activeIndex: id
      })
    } else {
      this.setState({
        activeIndex: null
      })
    }
  }

  handleSidebarMouseEnter = id => {
    if (id !== this.state.hoveredMenuItem) {
      this.setState({
        hoveredMenuItem: id
      })
    } else {
      this.setState({
        hoveredMenuItem: null
      })
    }
  }

  handleActiveItem = url => {
    this.setState({
      activeItem: url
    })
  }

  render() {
    let {
      visibilityState,
      toggleSidebarMenu,
      sidebarHover,
      toggle,
      color,
      sidebarVisibility,
      activeTheme,
      collapsed,
      activePath,
      sidebarState,
      currentLang,
      permission,
      currentUser,
      collapsedMenuPaths
    } = this.props

    let {
      menuShadow,
      activeIndex,
      hoveredMenuItem,
      activeItem,
      ScrollbarTag
    } = this.state

    let scrollShadow = (container, dir) => {
      if (container && dir === "up" && container.scrollTop >= 100) {
        this.setState({menuShadow: true})
      } else if (container && dir === "down" && container.scrollTop < 100) {
        this.setState({menuShadow: false})
      } else {

      }
    }

    return (
      <ContextLayout.Consumer>
        {context => {
          let dir = context.state.direction
          return (
            <React.Fragment>
              <Row>
                <Col>
                  <SubscribedAppsSideBar
                    width={this.state.width}
                    sidebarState={sidebarState}
                    visibilityState={visibilityState}
                    applications={this.props.applications}
                  />
                  <MainAppSideBar
                    appId={this.props.appId}
                    activeTheme={activeTheme}
                    toggle={toggle}
                    color={color}
                    dir={dir}
                    sidebarWidth={this.state.width}
                    sidebarState={sidebarState}
                    sidebarVisibilityState={visibilityState}
                    sidebarVisibility={sidebarVisibility}
                    sidebarHover={sidebarHover}
                    toggleSidebarMenu={toggleSidebarMenu}
                    collapsed={collapsed}
                    sidebarCollapsed={collapsed}
                    menuShadow={menuShadow}
                    activePath={activePath}
                    ScrollbarTag={ScrollbarTag}
                    scrollShadow={scrollShadow}
                    hoveredMenuItem={hoveredMenuItem}
                    activeIndex={activeIndex}
                    activeItem={activeItem}
                    currentLang={currentLang}
                    permission={permission}
                    currentUser={currentUser}
                    collapsedMenuPaths={collapsedMenuPaths}
                    deviceWidth={this.props.deviceWidth}
                    handleSidebarMouseEnter={this.handleSidebarMouseEnter}
                    handleActiveItem={this.handleActiveItem}
                    setActiveIndex={this.changeActiveIndex}
                  />
                </Col>
              </Row>
            </React.Fragment>
          )
        }}
      </ContextLayout.Consumer>
    )
  }
}

const mapStateToProps = state => {
  return {
    currentUser: state.auth.login.userRole,
    applications: state.app
  }
}

export default connect(mapStateToProps, {
  getApplications
})(Sidebar)
