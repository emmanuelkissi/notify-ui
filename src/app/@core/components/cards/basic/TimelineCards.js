import React from "react"
import { Card, CardHeader, CardTitle, CardBody, Row, Col } from "reactstrap"
import { Plus, AlertCircle, Check } from "react-feather"

class TimelineCards extends React.Component {
  render() {
    return (
      <Row>
        <Col >
          <Card>
            <CardHeader>
              <CardTitle>Historique des activités</CardTitle>
            </CardHeader>
            <CardBody>
              <ul className="activity-timeline timeline-left list-unstyled">
                <li>
                  <div className="timeline-icon bg-primary">
                    <Plus size="18" />
                  </div>
                  <div className="timeline-info">
                    <p className="font-weight-bold">Tâche ajoutée</p>
                    <span>
                     RIb :ffejo363 vous avez lancé une recuperation sur ce compte
                    </span>
                  </div>
                  <small className="">il y'a 4 jours</small>
                </li>
                <li>
                  <div className="timeline-icon bg-warning">
                    <AlertCircle size="18" />
                  </div>
                  <div className="timeline-info">
                    <p className="font-weight-bold">Tâche modifié</p>
                    <span>changement sur la date dont on recpera l'opération</span>
                  </div>
                  <small className="">il y'a 5 jours</small>
                </li>
                <li>
                  <div className="timeline-icon bg-success">
                    <Check size="18" />
                  </div>
                  <div className="timeline-info">
                    <p className="font-weight-bold">Tâche terminé</p>
                    <span>l'action faite sur le compte 47596MACON est terminé</span>
                  </div>
                  <small className="">il y'a 6 jours</small>
                </li>
              </ul>
            </CardBody>
          </Card>
        </Col>
      </Row>
    )
  }
}
export default TimelineCards
