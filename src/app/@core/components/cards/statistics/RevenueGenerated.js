import React from "react"
import StatisticsCard from "../../../../@vuexy/statisticsCard/StatisticsCard"
import { Package } from "react-feather"
import { revenueGeneratedSeries, revenueGenerated } from "./StatisticsData"

class RevenueGenerated extends React.Component {
  render() {
    return (
      <StatisticsCard
        icon={<Package className="success" size={22} />}
        iconBg="success"
        stat="4"
        statTitle="Nombre de compte bancaire ajouté"
        options={revenueGenerated}
        series={revenueGeneratedSeries}
        type="area"
      />
    )
  }
}
export default RevenueGenerated
