import React from "react"
import { Card, CardBody, Progress, Button } from "reactstrap"
import { ChevronsRight } from "react-feather"

class Funds extends React.Component {
  render() {
    return (
      <Card>
        <CardBody>
          <h1 className="mb-0">
            <sup className="font-medium-3 mr-50">fcfa</sup>
            Pack souscris
          </h1><br/><br/>
          <small>
            <span className="text-muted">souscris le: </span>20-05-2020
          </small><br/><br/>
          <small>
            <span className="text-muted">expire le: </span>20-06-2020
          </small><br/><br/>
          <small>
            <span className="text-muted">somme deposé: </span>25 000 fcfa
          </small>
          <h5 className="mt-1">
            <span className="text-success">+5.2% ($956)</span>
          </h5>
          <Button.Ripple
            block
            color="primary"
            className="w-100 box-shadow-1 mt-2"
          >
            Ajouter un nouvel abonnement <ChevronsRight size={15} />
          </Button.Ripple>
          <hr className="my-2" />
          <small>Argent restant: 20 000 fcfa</small>
          <Progress className="mt-1 mb-2" color="success" value="50" />

          <small>Durée: 15 jours</small>
          <Progress className="mt-1" color="warning" value="50" />
        </CardBody>
      </Card>
    )
  }
}
export default Funds
