import mock from "../mock"
const users = [
  {
    id: 268,
    username: "02/06/2020",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-3.jpg"),
    email: "angelo@sashington.com",
    name: "1",
    dob: "28 January 1998",
    gender: "female",
    country: "Bolivia",
    role: "admin",
    status: "En cours",
    is_verified: true,
    department: "sales",
    company: "WinDon Technologies Pvt Ltd",
    mobile: "+65958951757",
    website: "https://rowboat.com/insititious/Angelo",
    languages_known: ["English", "Arabic"],
    contact_options: ["email", "message", "phone"],
    location: {
      add_line_1: "A-65, Belvedere Streets",
      add_line_2: "",
      post_code: "1868",
      city: "New York",
      state: "New York",
      country: "United States"
    },
    social_links: {
      twitter: "https://twitter.com/adoptionism744",
      facebook: "https://www.facebook.com/adoptionism664",
      instagram: "https://www.instagram.com/adopt-ionism744/",
      github: "https://github.com/madop818",
      codepen: "https://codepen.io/adoptism243",
      slack: "@adoptionism744"
    },
    permissions: {
      users: {
        read: true,
        write: false,
        create: false,
        delete: false
      },
      posts: {
        read: true,
        write: true,
        create: true,
        delete: true
      },
      comments: {
        read: true,
        write: false,
        create: true,
        delete: false
      }
    }
  },
  {
    id: 269,
    username: "05/02/2015",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-2.jpg"),
    email: "rubi@ortwein.com",
    name: "95",
    country: "Syria",
    role: "user",
    status: "blocked",
    is_verified: false,
    department: "development"
  },
  {
    id: 270,
    username: "06/06/2015",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-3.jpg"),
    email: "donnette@charania.com",
    name: "448",
    country: "Iraq",
    role: "staff",
    status: "deactivated",
    is_verified: true,
    department: "sales"
  },
  {
    id:271,
    username: "25/08/2015",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-12.jpg"),
    email: "ardith@duffett.com",
    name: "45",
    country: "45",
    role: "user",
    status: "En cours",
    is_verified: false,
    department: "sales"
  },
  {
    id: 272,
    username: "26/08/2015",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-16.jpg"),
    email: "antone@berman.com",
    name: "69",
    country: "India",
    role: "user",
    status: "blocked",
    is_verified: true,
    department: "sales"
  },
  {
    id: 273,
    username: "04/05/2016",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-18.jpg"),
    email: "maryann@gour.com",
    name: "89",
    country: "Solomon Islands",
    role: "user",
    status: "En cours",
    is_verified: true,
    department: "sales"
  },
  {
    id: 274,
    username: "01/03/2015",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-7.jpg"),
    email: "holli@vanduyne.com",
    name:"1",
    country: "Lebanon",
    role: "staff",
    status: "active",
    is_verified: true,
    department: "sales"
  },
  {
    id: 275,
    username: "06/09/2014",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-26.jpg"),
    email: "juanita@sartoris.com",
    name: "5",
    country: "Papua New Guinea",
    role: "staff",
    status: "active",
    is_verified: true,
    department: "management"
  },
  {
    id: 276,
    username: "06/09/2014",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-20.jpg"),
    email: "lia@morga.com",
    name: "3",
    country: "Malaysia",
    role: "user",
    status: "active",
    is_verified: true,
    department: "development"
  },
  {
    id: 306,
    username: "31/05/2011",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-19.jpg"),
    email: "carter@mendes.com",
    name: "0",
    country: "Armenia",
    role: "user",
    status: "active",
    is_verified: true,
    department: "sales"
  },
  {
    id: 307,
    username: "23/05/2018",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-24.jpg"),
    email: "helene@madden.com",
    name: "0",
    country: "Finland",
    role: "staff",
    status: "active",
    is_verified: true,
    department: "development"
  },
  {
    id: 308,
    username: "15/08/2019",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-13.jpg"),
    email: "ashton@calderone.com",
    name: "4",
    country: "Italy",
    role: "user",
    status: "active",
    is_verified: true,
    department: "sales"
  },
  {
    id: 309,
    username: "06/09/2019",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-14.jpg"),
    email: "robert@lyster.com",
    name: "5",
    country: "Turkey",
    role: "user",
    status: "active",
    is_verified: true,
    department: "sales"
  },
  {
    id: 310,
    username: "25/01/2018",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-22.jpg"),
    email: "delma@mewbourn.com",
    name: "4",
    country: "Honduras",
    role: "staff",
    status: "deactivated",
    is_verified: false,
    department: "development"
  },
  {
    id: 311,
    username: "30/05/2019",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-17.jpg"),
    email: "ja@alaibilla.com",
    name: "2",
    country: "Italy",
    role: "user",
    status: "active",
    is_verified: true,
    department: "sales"
  },
  {
    id: 312,
    username: "20/02/2015",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-22.jpg"),
    email: "delinda@rosentrance.com",
    name: "3",
    country: "Guinea",
    role: "user",
    status: "active",
    is_verified: true,
    department: "development"
  },
  {
    id: 313,
    username: "06/05/2016",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-18.jpg"),
    email: "danae@demeter.com",
    name: "4",
    country: "Gambia",
    role: "user",
    status: "deactivated",
    is_verified: true,
    department: "sales"
  },
  {
    id: 314,
    username: "04/07/2017",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-14.jpg"),
    email: "kattie@joffrion.com",
    name: "5",
    country: "Albania",
    role: "user",
    status: "blocked",
    is_verified: true,
    department: "management"
  },
  {
    id: 315,
    username: "02/02/2005",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-19.jpg"),
    email: "in@stjohns.com",
    name: "6",
    country: "North Korea",
    role: "user",
    status: "active",
    is_verified: false,
    department: "sales"
  },
  {
    id: 316,
    username: "08/07/2020",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-2.jpg"),
    email: "van@laferney.com",
    name: "45",
    country: "Finland",
    role: "staff",
    status: "active",
    is_verified: true,
    department: "development"
  },
  {
    id: 317,
    username: "02/11/2013",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-6.jpg"),
    email: "sylvia@maharrey.com",
    name: "8",
    country: "Turkmenistan",
    role: "staff",
    status: "deactivated",
    is_verified: true,
    department: "sales"
  }
]

// GET DATA
mock.onGet("/api/tasks/list").reply(200, users)
