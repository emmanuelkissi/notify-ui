import mock from "../mock"
const users = [
  {
    id: 268,
    username: "adoptionism744",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-3.jpg"),
    email: "angelo@sashington.com",
    name: "Angelo Sashington",
    dob: "28 January 1998",
    gender: "female",
    country: "Bolivia",
    role: "admin",
    status: "active",
    is_verified: true,
    department: "sales",
    company: "WinDon Technologies Pvt Ltd",
    mobile: "+65958951757",
    website: "https://rowboat.com/insititious/Angelo",
    languages_known: ["English", "Arabic"],
    contact_options: ["email", "message", "phone"],
    location: {
      add_line_1: "A-65, Belvedere Streets",
      add_line_2: "",
      post_code: "1868",
      city: "New York",
      state: "New York",
      country: "United States"
    },
    social_links: {
      twitter: "https://twitter.com/adoptionism744",
      facebook: "https://www.facebook.com/adoptionism664",
      instagram: "https://www.instagram.com/adopt-ionism744/",
      github: "https://github.com/madop818",
      codepen: "https://codepen.io/adoptism243",
      slack: "@adoptionism744"
    },
    permissions: {
      users: {
        read: true,
        write: false,
        create: false,
        delete: false
      },
      posts: {
        read: true,
        write: true,
        create: true,
        delete: true
      },
      comments: {
        read: true,
        write: false,
        create: true,
        delete: false
      }
    },
    created_at: "12/01/2020",
    created_by: "Ben"
  },
  {
    id: 269,
    username: "89",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-2.jpg"),
    email: "rubi@ortwein.com",
    name: "Rubi Ortwein",
    country: "Syria",
    role: "user",
    status: "blocked",
    is_verified: false,
    department: "development",
    created_at: "12/01/2020",
    created_by: "Ben"
  },
  {
    id: 270,
    username: "undivorced341",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-3.jpg"),
    email: "donnette@charania.com",
    name: "Donnette Charania",
    country: "Iraq",
    role: "staff",
    status: "deactivated",
    is_verified: true,
    department: "sales",
    created_at: "12/01/2020",
    created_by: "Ben"
  },
  {
    id: 271,
    username: "bumbo426",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-12.jpg"),
    email: "ardith@duffett.com",
    name: "Ardith Duffett",
    country: "Estonia",
    role: "user",
    status: "active",
    is_verified: false,
    department: "sales",
    created_at: "12/01/2020",
    created_by: "Ben"
  },
  {
    id: 272,
    username: "ectodactylism214",
    avatar: require("../../../../assets/img/portrait/small/avatar-s-16.jpg"),
    email: "antone@berman.com",
    name: "Antone Berman",
    country: "India",
    role: "user",
    status: "blocked",
    is_verified: true,
    department: "sales",
    created_at: "12/01/2020",
    created_by: "Ben"
  }
]

// GET DATA
mock.onGet("/api/users/list").reply(200, users)
