import mock from "../mock";
export let dataList1 = [
  {
    id: 1,
    order_status: "ouvert",
    name: "MACO15648",
    banque: 'GT BANK',
    nb_don: 1
  },
  {
    id: 2,
    order_status: "ouvert",
    name: "DIGIT EXPERT",
    banque: 0,
    nb_don: 1
  },
  {
    id: 3,
    order_status: "fermé",
    name: "DECLARONS",
    banque: 0,
    nb_don: 1
  },
  {
    id: 3,
    order_status: "ouvert",
    name: "RH EXPERT",
    banque: 0,
    nb_don: 1
  },
  {
    id: 3,
    order_status: "ouvert",
    name: "AKILCAB",
    banque: 0,
    nb_don: 1
  }
];

const determinePopularity = val => {
  if (val >= 75) return { popValue: val, color: "success" }
  else if (val < 75 && val >= 55) return { popValue: val, color: "primary" }
  else if (val < 55 && val >= 35) return { popValue: val, color: "warning" }
  else if (val < 35 && val >= 0) return { popValue: val, color: "danger" }
  else return { popValue: 0, color: "danger" }
};

// GET DATA
mock.onGet("/api/datalist1/initial-data").reply(response => {
  return [200, dataList1]
});

mock.onGet("/api/datalist1/data").reply(response => {
  let { page, perPage } = response
  let totalPages = Math.ceil(dataList1.length / perPage)
  if (page !== undefined && perPage !== undefined) {
    let calculatedPage = (page - 1) * perPage
    let calculatedPerPage = page * perPage
    return [
      200,
      { data: dataList1.slice(calculatedPage, calculatedPerPage), totalPages }
    ]
  } else {
    return [
      200,
      { data: dataList1.slice(0, 4), totalPages: Math.ceil(dataList1.length / 4) }
    ]
  }
});

// UPDATE DATA
mock.onPost("/api/datalist1/update-data").reply(request => {
  let data = JSON.parse(request.data).obj
  dataList1.map(item => {
    if (item.id === data.id) {
      let popularity = determinePopularity(data.popularity.popValue)
      return Object.assign(item, { ...data, popularity })
    } else {
      return item
    }
  })
  return [200]
});

// Add DATA
mock.onPost("/api/datalist1/add-data").reply(request => {
  let data = JSON.parse(request.data).obj
  let highestId = Math.max.apply(
    Math,
    dataList1.map(i => i.id)
  )
  dataList1.unshift({
    ...data,
    id: highestId + 1,
    popularity: determinePopularity(data.popularity.popValue)
  })
  return [200]
});

// DELETE DATA
mock.onPost("/api/datalist1/delete-data").reply(request => {
  let data = JSON.parse(request.data).obj
  let index = dataList1.findIndex(item => item.id === data.id)
  dataList1.splice(index, 1)
  return [200]
});

// DELETE SELECTED DATA
mock.onPost("/api/datalist1/delete-selected").reply(request => {
  let data = JSON.parse(request.data).arr
  let reducedArray
  ;[dataList1, data].reduce((a, b) => {
    let c = b.map(j => j.id)
    return (reducedArray = a.filter(i => !c.includes(i.id)))
  })
  dataList1 = reducedArray
  return [200]
});
