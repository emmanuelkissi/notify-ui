import mock from "../mock"

let data = [

    {
      value: "Tommy Sicilia",  
      label: "Tommy Sicilia",
    },
    {
      value: "Tressa Gass",
      label: "Tressa Gass",    
    },  
    {
      value:"Ardis Balderson",
      label: "Ardis Balderson", 
    },
    {
      value: "Dagmar Mallall",
      label: "Dagmar Mallall", 
    },
    {
      value: "Nada MacGaughy",
      label: "Nada MacGaughy",
    },
    {
      value: "Dalila Ouldcott",
      label: "Dalila Ouldcott",
    }
  ]

//Get profile
mock.onGet("/api/datalist/banque").reply(200, data)

