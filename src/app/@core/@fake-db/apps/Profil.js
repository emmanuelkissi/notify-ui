import mock from "../mock"

let data = [

    {
      id: 1,  
      name: "Tommy Sicilia",
      img: require("../../../../assets/img/portrait/small/avatar-s-1.jpg"),
      time: "10jours",
     
    },
    {
      id: 2,
      name: "Tressa Gass",
      img: require("../../../../assets/img/portrait/small/avatar-s-2.jpg"),
      time: "hier",
    },
    
    {
      id: 27,
      name: "Ardis Balderson",
      img: require("../../../../assets/img/portrait/small/avatar-s-18.jpg"),
      time: "une semaine",
      
    },
    {
      id: 28,
      name: "Dagmar Mallall",
      to: ["johndoe@furl.net"],
      img: require("../../../../assets/img/portrait/small/avatar-s-4.jpg"),
      time: "10:55:00",
      day: "Aug 19",
      year: "2020",
      
    },
    {
      id: 29,
      name: "Nada MacGaughy",
      to: ["johndoe@cnet.com"],
      img: require("../../../../assets/img/portrait/small/avatar-s-18.jpg"),
      time: "10:55:00",
      day: "Aug 30",
      year: "2020",
     
    },
    {
      key: 30,
      name: "Dalila Ouldcott",
      to: ["johndoe@github.io"],
      img: require("../../../../assets/img/portrait/small/avatar-s-19.jpg"),
      time: "10:55:00",
      day: "Aug 30",
      year: "2020",

    }
  ]

//Get profile
mock.onGet("/api/datalist/profil").reply(200, data)

// Get Mails
/*mock.onGet("/api/email/mails").reply(request => {
  const filter = request.params.filter

  const filteredEmails = data.emails
    .filter(email => {
      if (filter === "inbox" || !["inbox", "sent", "draft", "starred", "trash", "spam"].includes(filter))
        return email.mailFolder === "inbox"
      if (filter === "sent") return email.mailFolder === "sent"
      if (filter === "draft") return email.mailFolder === "draft"
      if (filter === "starred")
        return email.isStarred && email.mailFolder !== "trash"
      if (filter === "trash") return email.mailFolder === "trash"
      if (filter === "spam") return email.mailFolder === "spam"
      else return email.mailFolder !== "trash" && email.labels.includes(filter)
    })
    .reverse()
  return [200, JSON.parse(JSON.stringify(filteredEmails))]
})*/

// Set starred
/*mock.onPost("/api/email/set-starred").reply(request => {
  const mailId = JSON.parse(request.data).mailId
  data.emails.find(mail => mail.id === mailId).isStarred !== true
    ? (data.emails.find(mail => mail.id === mailId).isStarred = true)
    : (data.emails.find(mail => mail.id === mailId).isStarred = false)
  return [200]
})

// Change Folders
mock.onPost("/api/email/move-mails").reply(request => {
  const reponseData = JSON.parse(request.data)
  const { selectedEmails, mailFolder } = reponseData

  // eslint-disable-next-line
  data.emails.find(mail => {
    if (selectedEmails.includes(mail.id)) mail.mailFolder = mailFolder
  })

  return [200]
})

// Mark Unread
mock.onPost("api/email/mark-unread").reply(request => {
  const mailsToUpdate = JSON.parse(request.data).emailIds

  mailsToUpdate.forEach(mailId => {
    const mailIndex = data.emails.findIndex(mail => mail.id === mailId)
    data.emails[mailIndex].unread = JSON.parse(request.data).unreadFlag
  })

  return [200]
})

// Set Labels
mock.onPost("api/email/set-labels").reply(request => {
  const label = JSON.parse(request.data).label
  const mailsToUpdate = JSON.parse(request.data).emailIds

  mailsToUpdate.forEach(mailId => {
    const mailIndex = data.emails.findIndex(mail => mail.id === mailId)
    const labelIndex = data.emails[mailIndex].labels.indexOf(label)

    if (labelIndex === -1) data.emails[mailIndex].labels.push(label)
    else data.emails[mailIndex].labels.splice(labelIndex, 1)
  })

  return [200]
})*/