import mock from "../mock"
import jwt from "jsonwebtoken"

let users = [
  {
    id:"notify-test-00102545",
    first_name:"Suini",
    last_name:"Olive",
    email:"suiniolive58@gmail.com",
    username:"skao",
    password:"silence02",
    mobile:"+225 58 04 81 55",
    created_at:"06/07/2020 06:12",
    updated_at:"",
    created_by:"suiniolive58@gmail.com",
    updated_by:"",
},
{
  id:"notify-test-51282125",
  first_name:"Jean",
  last_name:"Akila",
  email:"Akila@gmail.com",
  username:"akil",
  password:"silence01",
  mobile:"+225 58 04 81 55",
  created_at:"05/07/2020 06:12",
  updated_at:"",
  created_by:"Akila@gmail.com",
  updated_by:"",
}
];

const jwtConfig = {
  "secret": "dd5f3089-40c3-403d-af14-d0c228b05cb4",
  "expireTime": 8000
};

// POST: Check User Login Details and return user
mock.onPost("/api/authenticate/login/user").reply(request => {
  let {email, password} = JSON.parse(request.data);
  let error = "Something went wrong";

  const user = users.find(user => user.email === email && user.password === password);

  if (user) {

    try {

      const accessToken = jwt.sign({id: user.id}, jwtConfig.secret, {expiresIn: jwtConfig.expireTime})

      const userData = Object.assign({}, user, {loggedInWith: "jwt"})

      delete userData.password

      const response = {
        user: userData,
        accessToken: accessToken
      }

      return [200, response]

    } catch (e) {
      error = e
    }
  } else {
    error = "Email Or Password Invalid"
  }

  return [200, {error}]
})

mock.onPost("/api/authenticate/register/user").reply(request => {
  
  if (request.data.length > 0) {
   
    let {email, first_name, last_name,first_password,second_password,mobile} = JSON.parse(request.data)
    const isEmailAlreadyInUse = users.find((user) => user.email === email)
    const error = {
      email: isEmailAlreadyInUse ? 'Cet Email est déjà utiliser.' : null,
    
      content: mobile  === "" ||first_name === "" ||  last_name==="" || first_password==="" || second_password===""? 'Un ou plusieurs champs sont vide.' : null,
      password: first_password!==second_password ? 'Mot de passe non concordantes.' : null,
    }

    if (!error.content && !error.email && !error.password) {
     
      let userData = {
        first_name:first_name,
        last_name:last_name,
        email: email,
        mobile:mobile,
        password: first_password,
       
      }

      // Add user id
      const length = users.length
      let lastIndex = 0
      if (length) {
        lastIndex = users[length - 1].id
      }
      let position =length+1
      userData.id = lastIndex + position

      users.push(userData)

      const accessToken = jwt.sign({id: userData.id}, jwtConfig.secret, {expiresIn: jwtConfig.expireTime})

      let user = Object.assign({}, userData)
      delete user['password']
      const response = {user: user, accessToken: accessToken}
    
      return [200, response]
    } else {
      let errors = error.email +" "+error.content+" "+error.password;
      return [200, {errors}]
    }

  }

})

mock.onPost('/api/authenticate/refresh-token').reply((request) => {

  const {accessToken} = JSON.parse(request.data)

  try {
    const {id} = jwt.verify(accessToken, jwtConfig.secret)

    let userData = Object.assign({}, users.find(user => user.id === id))

    const newAccessToken = jwt.sign({id: userData.id}, jwtConfig.secret, {expiresIn: jwtConfig.expiresIn})

    delete userData['password']
    const response = {
      userData: userData,
      accessToken: newAccessToken
    }

    return [200, response]
  } catch (e) {
    const error = "Invalid access token"
    return [401, {error}]
  }
})
