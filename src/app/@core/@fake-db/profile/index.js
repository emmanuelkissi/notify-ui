import mock from "../mock"

const profiles = [
 
  {
    id: 2,
    avatar: require("../../../../assets/img/portrait/small/avatar-s-24.jpg"),
    entity_name: "Akiltechnologies",
    role: "user",
    
    is_verified: true,
    actors_nb: 12,
      status: "Operationnel",
      percentage: "100",
      start_date: "14:58 26/07/2018",
      type: "Associé"
  }
   /*{
    id: 1,
    avatar: require("../../../../assets/img/portrait/small/avatar-s-24.jpg"),
    entity_name: "Akilcab",
    role: "user",
    status: "active",
    is_verified: true
  },*/
]

const profileToken = {
  access_token: "azsbbsqshratytsyuasbn2345678shqbsqbsh345"
}

mock
  .onGet("/api/profiles")
  .reply(200, {data: profiles})

mock
  .onGet("/api/profiles/1/token")
  .reply(200, {data: profileToken})


mock
  .onPost("/api/profiles/new")
  .reply(request =>{
    let { profilname,
    profilpassword} = JSON.parse(request.data)
    let prof={
    id:profiles.length+ 2,
    avatar: require("../../../../assets/img/portrait/small/avatar-s-24.jpg"),
    entity_name: profilname,
    role: "user",
    is_verified: true,
    password:profilpassword,
    actors_nb: 12,
    status: "Operationnel",
    percentage: "20",
    start_date: "14:58 26/07/2018",
    type: "Associé"
    }
    profiles.push(prof) 
    let data ="save";
    return [200, data]
   }
  )

