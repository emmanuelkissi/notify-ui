# Notify-ui

Notify-ui or Akil Push-ui is the ui version of Akilpush which is a real-time notification application.


## Installation
After cloning the project, go to the current directory and make the following commands to install all the dependencies.

```bash
npm install 
```

## Usage

This project is the user interface of Notify or Akilpush, therefore the modules will be linked via the Notify API.
Modules.
#Email Sending
    To update after finishing
#SMS sending
    To update after finishing
#Push Web submissions
    To update after finishing
#Taking into account customer packages
    To update after finishing
#Statistics (On interactions and balance)
    To update after finishing

## Contributing
    
When contributing to this repository, please first discuss the change you wish to make via issue, email, or any other method with the owners of this repository before making a change.

Please note we have a code of conduct, please follow it in all your interactions with the project.

## License
[MIT](https://choosealicense.com/licenses/mit/)